from typing import Annotated

from fastapi import Depends, HTTPException, status
import jwt
from jwt.exceptions import InvalidTokenError
from pydantic import ValidationError
from sqlmodel import Session
from app.database import engine
from fastapi.security import OAuth2PasswordBearer
from app.config import settings
from app.models import TokenPayload, User


def get_session(args):
    with Session(engine) as session:
        yield session


sessionDep = Annotated[Session, Depends(get_session)]
reusable_oauth2 = OAuth2PasswordBearer(tokenUrl="login")

TokenDep = Annotated[str, Depends(reusable_oauth2)]


def get_current_user(session: sessionDep, token: TokenDep):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        # username: str = payload.get("sub")
        # if username is None:
        #     raise credentials_exception
        # token_data = TokenData(username=username)

        token_data = TokenPayload(**payload)
    except (InvalidTokenError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
        # raise credentials_exception
    # user = get_user(fake_users_db, username=token_data.username)
    # username = token_data.username

    user = session.get(User, token_data.sub)
    # statement = select(User).where(User.username == username)
    # user = session.exec(statement).first()
    if user is None:
        raise credentials_exception
    return user


CurrentUser = Annotated[User, Depends(get_current_user)]
