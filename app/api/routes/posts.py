from typing import Any
from fastapi import APIRouter, Query, status
from fastapi.responses import JSONResponse
from sqlalchemy import func
from sqlmodel import select
from app.api.deps import CurrentUser, sessionDep
from app.models import (
    Message,
    Post,
    PostCreate,
    PostPublic,
    PostUpdate,
    PostsPublic,
)

router = APIRouter()


@router.get("/", response_model=PostsPublic)
async def list_posts(
    *,
    session: sessionDep,
    offset: int = 0,
    limit: int = Query(default=100, le=100),
):
    posts = session.exec(
        select(Post).offset(offset).limit(limit).order_by(Post.id.desc())
    ).all()
    count = select(func.count()).select_from(Post)
    count = session.exec(count).one()

    return PostsPublic(data=posts, count=count)


@router.post(
    "/store/",
    response_model=PostPublic,
    status_code=status.HTTP_201_CREATED,
    response_class=JSONResponse,
)
async def store(*, session: sessionDep, post: PostCreate, current_user: CurrentUser):
    db_post = Post.model_validate(post, update={"user_id": current_user.id})
    session.add(db_post)
    session.commit()
    session.refresh(db_post)
    return db_post


@router.put("/update/{id}", response_model=PostPublic)
def update_post(
    *, session: sessionDep, current_user: CurrentUser, id: int, post: PostUpdate
) -> Any:

    db_post = session.get(Post, id)
    if not db_post:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The post with this id does not exist in the system",
            },
        )
    if db_post.user_id != current_user.id:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Not enough permissions",
            },
        )

    update_dict = post.model_dump(exclude_unset=True)
    db_post.sqlmodel_update(update_dict)
    session.add(db_post)
    session.commit()
    session.refresh(db_post)
    return db_post


@router.delete("/delete/{id}")
def delete_item(session: sessionDep, current_user: CurrentUser, id: int) -> Message:

    db_post = session.get(Post, id)
    if not db_post:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The post with this id does not exist in the system",
            },
        )
    if db_post.user_id != current_user.id:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Not enough permissions",
            },
        )
    session.delete(db_post)
    session.commit()
    return Message(message="Item deleted successfully")


@router.put("/update-like/{id}", response_model=PostPublic)
def update_post(*, session: sessionDep, current_user: CurrentUser, id: int) -> Any:
    db_post = session.get(Post, id)
    if not db_post:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The post with this id does not exist in the system",
            },
        )

    db_post.sqlmodel_update({"likes": (db_post.likes + 1)})
    session.add(db_post)
    session.commit()
    session.refresh(db_post)
    return db_post


@router.put("/update-dislike/{id}", response_model=PostPublic)
def update_post(*, session: sessionDep, current_user: CurrentUser, id: int) -> Any:
    db_post = session.get(Post, id)
    if not db_post:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The post with this id does not exist in the system",
            },
        )

    db_post.sqlmodel_update({"dislikes": (db_post.dislikes + 1)})
    session.add(db_post)
    session.commit()
    session.refresh(db_post)
    return db_post


@router.get("/tag/{tag}", response_model=PostsPublic)
async def view_posts_by_tag(session: sessionDep, tag: str):

    posts = (
        session.query(Post).order_by(Post.id.desc()).filter(Post.tags.any(tag)).all()
    )

    count = select(func.count()).select_from(Post).filter(Post.tags.any(tag))
    count = session.exec(count).one()
    return PostsPublic(data=posts, count=count)
