from datetime import timedelta
from typing import Annotated, Any
from fastapi import APIRouter, Depends, status
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security import OAuth2PasswordRequestForm

from sqlmodel import select
from app.api.deps import sessionDep
from passlib.context import CryptContext

from app.core.auth import (
    create_access_token,
    generate_password_reset_token,
    generate_reset_password_email,
    get_password_hash,
)
from app.models import Message, NewPassword, Token, User
from app.utils import send_email, verify_password_reset_token


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

router = APIRouter()


@router.post("/")
async def login(
    session: sessionDep, form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
) -> Token:
    statement = select(User).where(User.username == form_data.username)
    user_registed = session.exec(statement).first()
    if not user_registed:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Incorrect username or password",
            },
        )
    validate_password = pwd_context.verify(form_data.password, user_registed.password)
    if not validate_password:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Incorrect username or password",
            },
        )
    access_token_expires = timedelta(minutes=60 * 24 * 8)
    access_token = create_access_token(
        data={"sub": user_registed.id}, expires_delta=access_token_expires
    )

    return Token(access_token=access_token)


@router.post("/password-recovery/{email}")
def password_recovery(email: str, session: sessionDep) -> Message:
    statement = select(User).where(User.email == email)
    user = session.exec(statement).first()
    if not user:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The user with this email does not exist in the system.",
            },
        )
    password_reset_token = generate_password_reset_token(email=email)
    email_data = generate_reset_password_email(
        email_to=user.email, email=email, token=password_reset_token
    )
    send_email(
        email_to=user.email,
        subject=email_data.subject,
        html_content=email_data.html_content,
    )
    return Message(message="Password recovery email sent")


@router.post("/reset-password/")
def reset_password(session: sessionDep, body: NewPassword) -> Message:
    email = verify_password_reset_token(token=body.token)

    if not email:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Invalid token.",
            },
        )

    statement = select(User).where(User.email == email)
    user = session.exec(statement).first()
    if not user:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The user with this email does not exist in the system.",
            },
        )

    hashed_password = get_password_hash(password=body.new_password)
    user.password = hashed_password
    session.add(user)
    session.commit()
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            "message": f"Password updated successfully.",
        },
    )


@router.post(
    "/password-recovery-html-content/{email}",
    response_class=HTMLResponse,
)
def recover_password_html_content(email: str, session: sessionDep) -> Any:

    statement = select(User).where(User.email == email)
    user = session.exec(statement).first()

    if not user:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "message": f"The user with this email does not exist in the system.",
            },
        )
    password_reset_token = generate_password_reset_token(email=email)
    email_data = generate_reset_password_email(
        email_to=user.email, email=email, token=password_reset_token
    )

    return HTMLResponse(content=email_data.html_content)
