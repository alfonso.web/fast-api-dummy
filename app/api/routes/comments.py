from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from app.api.deps import CurrentUser, sessionDep
from app.models import (
    Comment,
    CommentCreate,
    CommentPublic,
    CommentUpdate,
    Message,
)

router = APIRouter()


@router.post(
    "/store",
    response_model=CommentPublic,
)
async def store(
    *, session: sessionDep, comment: CommentCreate, current_user: CurrentUser
):
    db_comment = Comment.model_validate(comment, update={"user_id": current_user.id})
    session.add(db_comment)
    session.commit()
    session.refresh(db_comment)
    return db_comment


@router.patch(
    "/update/{comment_id}",
    response_class=JSONResponse,
)
def update(
    session: sessionDep,
    comment_id: int,
    comment: CommentUpdate,
    current_user: CurrentUser,
):
    db_comment = session.get(Comment, comment_id)
    if not db_comment:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content={
                "message": f"The comment with this id does not exist in the system",
            },
        )

    if db_comment.user_id != current_user.id:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Not enough permissions",
            },
        )

    db_comment.sqlmodel_update({"body": comment.body})

    session.add(db_comment)
    session.commit()
    session.refresh(db_comment)
    return db_comment


@router.delete(
    "/delete/{comment_id}",
    response_class=JSONResponse,
)
def delete(
    session: sessionDep,
    comment_id: int,
    current_user: CurrentUser,
) -> Message:
    db_comment = session.get(Comment, comment_id)
    if not db_comment:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content={
                "message": f"The comment with this id does not exist in the system",
            },
        )

    if db_comment.user_id != current_user.id:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Not enough permissions",
            },
        )

    session.delete(db_comment)
    session.commit()
    return Message(message="Comment deleted successfully")
