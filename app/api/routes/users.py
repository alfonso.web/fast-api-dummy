from typing import Any
from fastapi import APIRouter, status
from fastapi.responses import JSONResponse


from sqlmodel import or_, select
from app.api.deps import CurrentUser, sessionDep


from app.core.auth import get_password_hash, verify_password
from app.models import (
    Message,
    User,
    UserPublic,
    UserRegister,
    UserUpdateEmail,
    UserUpdatePassword,
)

router = APIRouter()


@router.get("/me")
async def read_user_me(current_user: CurrentUser):
    return current_user


@router.post(
    "/store",
    response_model=UserPublic,
    status_code=status.HTTP_201_CREATED,
    response_class=JSONResponse,
)
async def store(*, session: sessionDep, user: UserRegister):
    statement = select(User).where(
        or_(User.email == user.email, User.username == user.username)
    )
    user_registed = session.exec(statement).first()

    if user_registed:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"There is already a registered user with the email {user.email} or username {user.username}.",
            },
        )
    else:
        hash_password = get_password_hash(user.password)
        extra_data = {"password": hash_password}
        db_user = User.model_validate(user, update=extra_data)
        session.add(db_user)
        session.commit()
        session.refresh(db_user)
        return db_user


@router.patch(
    "/update/me",
    response_model=UserPublic,
    status_code=status.HTTP_200_OK,
)
async def update(session: sessionDep, user: UserUpdateEmail, current_user: CurrentUser):
    if user.email:
        statement = select(User).where(User.email == user.email)
        existing_user = session.exec(statement).first()
        if existing_user and existing_user.id != current_user.id:
            return JSONResponse(
                status_code=status.HTTP_409_CONFLICT,
                content={
                    "message": f"User with this email already exists",
                },
            )
        user_data = user.model_dump(exclude_unset=True)
        current_user.sqlmodel_update(user_data)
        session.add(current_user)
        session.commit()
        session.refresh(current_user)

        return current_user


@router.patch(
    "/update/me/password",
    response_model=Message,
    status_code=status.HTTP_200_OK,
)
def update_password_me(
    *, session: sessionDep, body: UserUpdatePassword, current_user: CurrentUser
) -> Any:
    """
    Update own password.
    """
    if not verify_password(body.current_password, current_user.password):
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"Incorrect password",
            },
        )

    if body.current_password == body.new_password:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={
                "message": f"New password cannot be the same as the current one",
            },
        )

    hashed_password = get_password_hash(body.new_password)
    current_user.password = hashed_password
    session.add(current_user)
    session.commit()
    return Message(message="Password updated successfully")


# pendiente

# @router.delete(
#     "/me",
#     response_model=Message,
#     status_code=status.HTTP_200_OK,
# )
# def delete_user_me(session: sessionDep, current_user: CurrentUser) -> Any:
#     """
#     Delete own user.
#     """
#     if current_user.is_superuser:
#         raise HTTPException(
#             status_code=403, detail="Super users are not allowed to delete themselves"
#         )
#     statement = delete(Item).where(col(Item.owner_id) == current_user.id)
#     session.exec(statement)  # type: ignore
#     session.delete(current_user)
#     session.commit()
#     return Message(message="User deleted successfully")


# @router.get(
#     "/index",
#     dependencies=[Depends(get_current_user)],
#     response_model=UsersPublic,
#     status_code=status.HTTP_200_OK,
# )
# async def index(
#     session: sessionDep, offset: int = 0, limit: int = Query(default=400, le=300)
# ):

#     users = crud_users.list(session=session, offset=offset, limit=limit)
#     return users


# @router.delete(
#     "/delete/{userId}",
#     status_code=status.HTTP_200_OK,
# )
# async def destroy(session: sessionDep, userId: int):
#     user = session.get(User, userId)
#     if not user:
#         return JSONResponse(
#             status_code=status.HTTP_404_NOT_FOUND,
#             content={
#                 "message": f"The user with this id does not exist in the system",
#             },
#         )
#     session.delete(user)
#     session.commit()
#     return JSONResponse(
#         status_code=status.HTTP_200_OK,
#         content={
#             "message": f"User deleted successfully",
#         },
#     )
