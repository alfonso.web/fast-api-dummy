INSERT INTO
    "comment" (
        "body",
        "post_id",
        "likes",
        "user_id"
    )
VALUES (
        'This is some awesome thinking!',
        150,
        3,
        105
    ),
    (
        'What terrific math skills you''re showing!',
        46,
        4,
        183
    ),
    (
        'You are an amazing writer!',
        150,
        2,
        1
    ),
    (
        'Wow! You have improved so much!',
        31,
        1,
        89
    ),
    ('Nice idea!', 100, 1, 149),
    (
        'You are showing excellent understanding!',
        18,
        5,
        110
    ),
    (
        'This is clear, concise, and complete!',
        123,
        1,
        4
    ),
    (
        'What a powerful argument!',
        16,
        0,
        145
    ),
    (
        'I knew you could do it!',
        34,
        3,
        207
    ),
    ('Wonderful ideas!', 87, 0, 86),
    (
        'It was a pleasure to grade this!',
        156,
        8,
        162
    ),
    (
        'Keep up the incredible work!',
        119,
        10,
        90
    ),
    (
        'My goodness, how impressive!',
        108,
        10,
        87
    ),
    (
        'You''re showing inventive ideas!',
        20,
        8,
        77
    ),
    (
        'You''ve shown so much growth!',
        6,
        2,
        17
    ),
    (
        'Interesting thoughts!',
        14,
        5,
        199
    ),
    (
        'I love your neat work!',
        12,
        7,
        155
    ),
    (
        'Doesn''t it feel good to do such great work?',
        145,
        6,
        134
    ),
    (
        'First-rate work!',
        178,
        1,
        203
    ),
    (
        'This is fascinating information!',
        176,
        4,
        39
    ),
    (
        'You inspire me!',
        111,
        0,
        113
    ),
    (
        'This is right on target!',
        9,
        5,
        113
    ),
    (
        'What an astounding observation!',
        33,
        1,
        155
    ),
    (
        'This is very well thought out!',
        121,
        1,
        169
    ),
    (
        'I can tell you''ve been practicing!',
        67,
        9,
        160
    ),
    (
        'You''ve come a long way!',
        79,
        7,
        59
    ),
    (
        'I can tell you''ve been paying attention!',
        55,
        9,
        57
    ),
    (
        'Reading this made my day!',
        45,
        8,
        177
    ),
    (
        'This is very perceptive!',
        31,
        2,
        168
    ),
    (
        'What an accomplishment!',
        126,
        8,
        89
    ),
    (
        'You make a great point here!',
        105,
        9,
        83
    ),
    (
        'I really like your creativity!',
        150,
        6,
        94
    ),
    (
        'You are an exceptional student!',
        77,
        7,
        171
    ),
    (
        'You have brilliant thoughts!',
        112,
        7,
        115
    ),
    (
        'This is beautiful!',
        86,
        7,
        83
    ),
    (
        'Dazzling examples!',
        13,
        9,
        174
    ),
    (
        'Vivid language choices!',
        47,
        5,
        12
    ),
    (
        'You express your ideas so well!',
        16,
        3,
        189
    ),
    (
        'This was a delight to read!',
        146,
        7,
        196
    ),
    (
        'This is a persuasive argument!',
        79,
        3,
        157
    ),
    (
        'You show an impressive grasp on this subject!',
        91,
        3,
        116
    ),
    ('You are gifted!', 89, 3, 83),
    (
        'You are so clever!',
        40,
        8,
        171
    ),
    (
        'What a great learner you are!',
        181,
        5,
        177
    ),
    (
        'I value these thoughts!',
        146,
        10,
        48
    ),
    (
        'You are such a motivated worker!',
        176,
        8,
        68
    ),
    (
        'You show great attention to detail!',
        53,
        1,
        115
    ),
    (
        'You are so artistic!',
        89,
        4,
        36
    ),
    (
        'I am so proud of you!',
        86,
        3,
        144
    ),
    (
        'Lovely handwriting!',
        123,
        2,
        74
    ),
    ('Great example!', 120, 4, 15),
    (
        'You worked so hard!',
        84,
        7,
        93
    ),
    (
        'You are a star!',
        162,
        3,
        115
    ),
    (
        'You learned so much!',
        117,
        6,
        86
    ),
    (
        'You are so smart!',
        50,
        1,
        29
    ),
    (
        'You''ve made a lot of progress!',
        58,
        8,
        35
    ),
    (
        'What bright thinking!',
        25,
        0,
        194
    ),
    (
        'You rocked this!',
        81,
        10,
        106
    ),
    ('Great thinking!', 15, 1, 74),
    (
        'You did your best!',
        10,
        10,
        18
    ),
    ('I love this!', 174, 10, 40),
    (
        'You can do hard things!',
        78,
        10,
        76
    ),
    (
        'You are talented!',
        56,
        1,
        98
    ),
    ('You amaze me!', 123, 6, 171),
    (
        'You discovered something new!',
        87,
        8,
        100
    ),
    (
        'I enjoyed reading this!',
        177,
        8,
        44
    ),
    (
        'You are so ambitious!',
        176,
        0,
        13
    ),
    (
        'I appreciate your hard work!',
        155,
        8,
        51
    ),
    (
        'This is magical work!',
        66,
        5,
        148
    ),
    ('You did it!', 133, 5, 78),
    (
        'You''ve achieved so much!',
        109,
        4,
        188
    ),
    (
        'You really challenged yourself!',
        143,
        6,
        84
    ),
    ('I admire you!', 23, 3, 52),
    (
        'You are unstoppable!',
        63,
        3,
        192
    ),
    (
        'You have great ideas!',
        104,
        7,
        164
    ),
    (
        'This really sparkles!',
        2,
        8,
        68
    ),
    (
        'What a great vision you have!',
        56,
        0,
        57
    ),
    (
        'You have really improved!',
        174,
        9,
        198
    ),
    (
        'You''ve really grown!',
        51,
        6,
        195
    ),
    (
        'You are a snappy problem-solver!',
        171,
        2,
        112
    ),
    (
        'This really shines!',
        97,
        7,
        146
    ),
    (
        'You make me want to learn more!',
        19,
        9,
        95
    ),
    (
        'This made me smile!',
        15,
        8,
        204
    ),
    ('You are a winner!', 3, 8, 14),
    (
        'I love your creativity!',
        78,
        8,
        90
    ),
    (
        'You are so intelligent!',
        167,
        4,
        30
    ),
    (
        'You should be proud!',
        12,
        2,
        80
    ),
    (
        'You have amazing potential!',
        49,
        0,
        28
    ),
    (
        'This is top-notch!',
        161,
        5,
        78
    ),
    (
        'You deserve a high five!',
        145,
        9,
        135
    ),
    (
        'Way to think it through!',
        133,
        9,
        38
    ),
    (
        'This blew me away!',
        159,
        3,
        150
    ),
    (
        'These are fabulous ideas!',
        1,
        7,
        190
    ),
    (
        'This gets my seal of approval!',
        189,
        0,
        197
    ),
    (
        'This is quality work!',
        88,
        7,
        174
    ),
    (
        'You reached your goal!',
        109,
        1,
        2
    ),
    (
        'Out of this world!',
        16,
        7,
        208
    ),
    (
        'You''re on top of it!',
        158,
        2,
        126
    ),
    (
        'I can tell this is your best effort!',
        49,
        9,
        166
    ),
    (
        'I love how motivated you are!',
        26,
        2,
        11
    ),
    (
        'You are so focused!',
        25,
        7,
        44
    ),
    (
        'I''m so lucky to grade your work!',
        128,
        5,
        81
    ),
    (
        'I love your enthusiasm!',
        12,
        7,
        21
    ),
    (
        'I think this is the best I''ve seen till now.',
        12,
        0,
        39
    ),
    (
        'Not enough for me, you are everything.',
        111,
        6,
        115
    ),
    (
        'Just when I couldn''t love you more. You posted this pic and my jaw dropped to the floor.',
        105,
        5,
        50
    ),
    (
        'You are a symbol of beauty.',
        1,
        7,
        131
    ),
    (
        'Surely you are the most beautiful woman alive, the most that I''ve seen.',
        11,
        0,
        206
    ),
    (
        'You mean the world to me.',
        108,
        6,
        109
    ),
    (
        'The word pretty is worthless without you.',
        17,
        8,
        6
    ),
    (
        'You are the definition of Beauty.',
        95,
        6,
        122
    ),
    (
        'Flawless outstanding eccentric lovely beauty.',
        56,
        9,
        184
    ),
    (
        'The stars, the moon, and the sun are minor to me since you sparkle brighter than all of them.',
        11,
        3,
        102
    ),
    (
        'You are graceful.',
        14,
        2,
        130
    ),
    (
        'Surely you would have been arrested if looking immensely beautiful was a crime.',
        167,
        1,
        2
    ),
    (
        'You''re an incredibly stunning, a really pretty, beautiful woman.',
        44,
        5,
        62
    ),
    (
        'I guess this is your best look, you look beautiful in this outfit.',
        151,
        7,
        16
    ),
    (
        'You look mesmerizing.',
        12,
        2,
        18
    ),
    (
        'I wonder how cute you would be looking when you sleep.',
        67,
        9,
        25
    ),
    (
        'You are so much lovely.',
        68,
        5,
        14
    ),
    (
        'I love your curly hair.',
        41,
        7,
        14
    ),
    (
        'Your dressing sense is appreciable.',
        22,
        8,
        16
    ),
    (
        'You are the sweetest girl I have ever seen.',
        72,
        2,
        15
    ),
    (
        'I love that straight hair of yours.',
        56,
        1,
        116
    ),
    (
        'You are pleasing.',
        122,
        10,
        143
    ),
    (
        'Just ''WOW'' for your natural beauty.',
        188,
        4,
        107
    ),
    (
        'I love how comfortable you are in your world.',
        65,
        8,
        154
    ),
    (
        'You are very much adorable.',
        39,
        2,
        37
    ),
    (
        'You look even more pretty without makeup.',
        55,
        0,
        63
    ),
    (
        'I feel so happy to be with you.',
        78,
        5,
        4
    ),
    (
        'You are my safest place.',
        124,
        2,
        29
    ),
    (
        'You look beautiful in that dress.',
        17,
        4,
        191
    ),
    (
        'I wish I had your makeup.',
        100,
        5,
        69
    ),
    (
        'You are stunning.',
        96,
        10,
        125
    ),
    (
        'That natural beauty tho.',
        33,
        6,
        32
    ),
    (
        'You have the most beautiful, sparkling eyes.',
        146,
        3,
        7
    ),
    (
        'You have the most beautiful smile.',
        22,
        0,
        6
    ),
    (
        'Your sense of fashion is great.',
        152,
        3,
        126
    ),
    (
        'I like your hair.',
        120,
        8,
        92
    ),
    (
        'You look more beautiful than in the photo.',
        117,
        4,
        98
    ),
    (
        'You look stunning.',
        157,
        4,
        144
    ),
    ('You''re classy.', 78, 4, 26),
    (
        'You''re very trendy.',
        81,
        4,
        110
    ),
    (
        'Your true colors are beautiful.',
        141,
        6,
        77
    ),
    (
        'Getting lost in your chroma can prove fatal.',
        99,
        7,
        130
    ),
    (
        'You''ve got a beautiful heart.',
        149,
        8,
        137
    ),
    (
        'Inside out; you are beautiful.',
        19,
        1,
        76
    ),
    (
        'You take my breath away.',
        54,
        4,
        15
    ),
    (
        'I love how expressive your eyes are.',
        14,
        2,
        37
    ),
    (
        'Your beauty is one of the things I like about you.',
        42,
        7,
        39
    ),
    (
        'Honestly, you are beautiful.',
        153,
        1,
        130
    ),
    (
        'You''re helping us sound lovely.',
        179,
        4,
        110
    ),
    (
        'The beauty lies within the soul.',
        186,
        6,
        55
    ),
    (
        'It''s refreshing, your beauty.',
        114,
        8,
        127
    ),
    (
        'You are a whisper of perfection.',
        19,
        7,
        168
    ),
    (
        'You''ve got lovely eyes.',
        88,
        2,
        161
    ),
    (
        'There are endless possibilities for your beauty.',
        56,
        10,
        147
    ),
    (
        'You are a true beauty epitome.',
        45,
        0,
        39
    ),
    (
        'You''re beautiful and enthusiastic.',
        145,
        5,
        142
    ),
    (
        'I see the power of your grace.',
        117,
        5,
        174
    ),
    (
        'You''re like the sun''s ray.',
        14,
        6,
        65
    ),
    (
        'You''re a divine present.',
        177,
        7,
        205
    ),
    (
        'Beauty is just one of the beautiful features you have.',
        36,
        10,
        87
    ),
    (
        'The expression is lovely.',
        46,
        8,
        44
    ),
    (
        'Sorry is overcome by the grace.',
        34,
        10,
        148
    ),
    (
        'You are true, beautiful and pure.',
        55,
        9,
        24
    ),
    (
        'You''re flawless.',
        146,
        3,
        1
    ),
    (
        'You have a beautiful smile.',
        22,
        10,
        105
    ),
    (
        'Your beauty is amazing.',
        24,
        10,
        71
    ),
    (
        'I see the beauty of your life.',
        181,
        9,
        51
    ),
    (
        'I think you''re fascinated.',
        104,
        10,
        27
    ),
    (
        'You''re energetic, aren''t ya?',
        179,
        4,
        67
    ),
    ('Impressive pic.', 155, 6, 26),
    (
        'Strong and confident.',
        145,
        4,
        60
    ),
    (
        'The picture is lit!',
        163,
        8,
        25
    ),
    (
        'Vibes everywhere!',
        34,
        8,
        86
    ),
    (
        'I love your straight hair with a slight flix.',
        23,
        9,
        10
    ),
    (
        'The most stunning thing I''ve seen today.',
        40,
        0,
        180
    ),
    (
        'Your smile makes everyone happy.',
        11,
        0,
        17
    ),
    (
        'You are so gorgeous.',
        84,
        6,
        189
    ),
    (
        'You made my day.',
        149,
        3,
        90
    ),
    (
        'This beauty has no boundaries.',
        132,
        8,
        82
    ),
    (
        'No one makes me smile more than you.',
        172,
        9,
        164
    ),
    (
        'Lady, you make me feel like a man.',
        111,
        10,
        167
    ),
    (
        'I just can''t stop wondering about you.',
        155,
        0,
        28
    ),
    (
        'When I hear your voice, my morning is all my day.',
        90,
        7,
        3
    ),
    (
        'I am now not thankful for anything aside from having you in my life.',
        179,
        7,
        152
    ),
    (
        'I can never get angry with you, your love is incompetent.',
        143,
        0,
        162
    ),
    (
        'If there is no “you”, then my life means nothing.',
        90,
        2,
        74
    ),
    (
        'I get attracted to you when you try to make me a better person.',
        58,
        9,
        207
    ),
    (
        'I want to see you.',
        76,
        4,
        47
    ),
    (
        'Hehe. cute monkey.',
        13,
        5,
        152
    ),
    (
        'You''re too short, just like your brain.',
        30,
        9,
        113
    ),
    (
        'Hey, you brainless cutie….',
        20,
        0,
        28
    ),
    (
        'I am still wondering how can god create this kind of bizarre human being who is cute, adorable at the same time has no brain.',
        88,
        7,
        181
    ),
    (
        'You are my precious fool.',
        4,
        8,
        39
    ),
    ('Hello idiot!', 14, 7, 98),
    (
        'What did I just see, that''s amazing, oh sorry it''s you!',
        2,
        5,
        150
    ),
    (
        'Cute little bitch.',
        79,
        4,
        117
    ),
    (
        'Surely you''d have used a photo editor.',
        141,
        3,
        100
    ),
    ('Nice make-up.', 77, 6, 13),
    (
        'Hey Guys! here you can get to know about all the latest updates of Instagram filters.',
        114,
        2,
        166
    ),
    ('Pretty enough.', 180, 6, 120),
    ('How Gorgeous.', 114, 0, 93),
    (
        'You are ravishing.',
        178,
        7,
        128
    ),
    (
        'Why are you so beautiful?',
        80,
        1,
        116
    ),
    (
        'You are amazing.',
        126,
        5,
        184
    ),
    ('Lit!', 168, 1, 180),
    (
        'Your eyes are like stars.',
        1,
        10,
        95
    ),
    (
        'You are just inexplicable.',
        4,
        3,
        158
    ),
    (
        'Damn, what are you made of?',
        133,
        3,
        93
    ),
    ('You seem pretty.', 30, 1, 20),
    (
        'You are engaging.',
        175,
        1,
        42
    ),
    (
        'This one''s stunning.',
        48,
        2,
        104
    ),
    (
        'Damn that cheeks.',
        169,
        10,
        43
    ),
    (
        'You are winsome.',
        134,
        9,
        50
    ),
    (
        'You are exquisite.',
        51,
        10,
        14
    ),
    (
        'Hello, Miss world.',
        18,
        6,
        146
    ),
    (
        'You are elegant.',
        141,
        2,
        67
    ),
    ('Mine.', 58, 5, 30),
    (
        'Your charm is never-ending.',
        126,
        1,
        200
    ),
    (
        'Your smile is beautiful.',
        92,
        2,
        83
    ),
    ('Nice outfit.', 38, 8, 165),
    (
        'Those earrings suit you.',
        65,
        8,
        128
    ),
    (
        'You are graceful.',
        31,
        8,
        11
    ),
    ('Cutiepie.', 152, 2, 189),
    ('So lovely.', 17, 5, 101),
    (
        'Your charm is irresistible.',
        131,
        10,
        205
    ),
    (
        'See your beauty.',
        167,
        8,
        165
    ),
    ('You are mine.', 42, 9, 127),
    ('My heartbeat.', 21, 4, 106),
    (
        'The most pretty girl around.',
        181,
        10,
        107
    ),
    (
        'Probably the next miss world.',
        175,
        0,
        78
    ),
    (
        'That innocent look tho.',
        50,
        0,
        146
    ),
    (
        'It seems your Demand will probably increase in the future, lmao.',
        48,
        2,
        65
    ),
    ('You spunky.', 165, 9, 23),
    (
        'Precious diamond.',
        135,
        5,
        126
    ),
    ('I respect you.', 41, 8, 121),
    ('Keep smiling.', 12, 7, 103),
    (
        'Where did you get that charm from?',
        150,
        6,
        83
    ),
    (
        'I''m sure your mother is the most beautiful lady in the world.',
        11,
        0,
        149
    ),
    (
        'I''ve heard about seven wonders of the world and the 8th just showed up!!',
        9,
        0,
        35
    ),
    (
        'You are wonderful.',
        101,
        1,
        89
    ),
    (
        'You are mind-blowing.',
        165,
        1,
        55
    ),
    ('Cool.', 165, 1, 46),
    (
        'You are Fantabulous.',
        96,
        2,
        173
    ),
    (
        'This pic is just Fantastic.',
        102,
        9,
        146
    ),
    (
        'Supercalifragilisticexpialidocious.',
        83,
        5,
        151
    ),
    (
        'Incredibly attractive.',
        30,
        9,
        108
    ),
    (
        'Aren''t you awesome?',
        70,
        8,
        57
    ),
    (
        'Your magnificence is irresistible.',
        58,
        3,
        51
    ),
    (
        'Such a charming post.',
        113,
        7,
        57
    ),
    ('Elegant pic.', 14, 2, 22),
    (
        'My words are less to portray this picture.',
        144,
        6,
        127
    ),
    (
        'Beauty lies inside for those who choose to see.',
        26,
        6,
        84
    ),
    (
        'The control of excellence lies inside the soul.',
        13,
        8,
        113
    ),
    (
        'This picture is worth a million words.',
        99,
        9,
        25
    ),
    (
        'Beauty is control; a grin is its sword.',
        18,
        9,
        74
    ),
    (
        'This looks exotic.',
        186,
        6,
        199
    ),
    (
        'I cherish how dynamic colors are within the picture.',
        104,
        4,
        88
    ),
    (
        'Such a beautiful picture looks great.',
        166,
        2,
        53
    ),
    (
        'Impressive picture.',
        187,
        2,
        117
    ),
    (
        'Your grin makes me Happy.',
        138,
        6,
        51
    ),
    (
        'Looking Flawless and this picture made my day.',
        157,
        1,
        117
    ),
    (
        'Your elegance is indescribable.',
        53,
        7,
        100
    ),
    (
        'Just divinely gorgeous.',
        77,
        10,
        74
    ),
    (
        'You''re an exceptionally wondrous and marvelous girl.',
        140,
        0,
        160
    ),
    (
        'Your looks make me insane.',
        90,
        2,
        56
    ),
    (
        'What a plenitude of magnificence.',
        131,
        4,
        46
    ),
    (
        'Grasp the wonderful mess that you simply are.',
        150,
        8,
        129
    ),
    (
        'Incredible characteristic excellence with parts of huge charm and astounding gorgeousness.',
        180,
        2,
        116
    ),
    (
        'The exemplification of beauty.',
        100,
        3,
        106
    ),
    (
        'Natural beauty with a brilliant heart.',
        119,
        5,
        156
    ),
    (
        'Lovely and more than rich!',
        25,
        0,
        16
    ),
    (
        'Mesmerizing Treasury house of beauty.',
        168,
        7,
        133
    ),
    (
        'That''s the icon to revere if you''re sitting out of gear.',
        143,
        3,
        103
    ),
    (
        'How can somebody be this beautiful.',
        90,
        6,
        125
    ),
    (
        'Dazzling and impressive queen!',
        177,
        2,
        18
    ),
    (
        'We all are favored to see your magnificence.',
        92,
        2,
        163
    ),
    (
        'You have got such an excellent smile.',
        23,
        4,
        29
    ),
    (
        'Your smile is fair enchanted and magnificent.',
        57,
        9,
        164
    ),
    (
        'Like a sparkling emerald you are rare and devine.',
        114,
        8,
        182
    ),
    (
        'You''ve got so charming and lovely grin that indeed God gives you each chance to smile.',
        45,
        9,
        83
    ),
    (
        'Keep grinning continuously expensive, you see so charming and lovely after you smile.',
        113,
        0,
        25
    ),
    (
        'What a Captivating capture!',
        164,
        2,
        19
    ),
    (
        'Such a super magnificent capture this one is.',
        78,
        7,
        204
    ),
    (
        'Such a charming capture.',
        131,
        7,
        14
    ),
    (
        'Btw for me you continuously were astounding; always are amazing and continuously gonna be astounding too.',
        129,
        3,
        178
    ),
    (
        'Well I think this is often my favorite posture of yours.',
        51,
        9,
        199
    ),
    (
        'The meaning of excellence lies in you.',
        81,
        7,
        70
    ),
    (
        'You are the light in the darkness.',
        42,
        4,
        53
    ),
    (
        'No one should be permitted to look that good, ok?',
        10,
        5,
        170
    ),
    (
        'You''re so charming, I can''t deal.',
        98,
        8,
        9
    ),
    (
        'Hello, you seem truly charming in this as always.',
        39,
        9,
        9
    ),
    (
        'That grin is priceless.',
        104,
        1,
        25
    ),
    (
        'You shake my world.',
        89,
        7,
        171
    ),
    (
        'You are my world.',
        36,
        6,
        23
    ),
    (
        'You know you''re pretty… lovely, amazing & extremely wonderful.',
        139,
        1,
        55
    ),
    (
        'My heart skips a beat each time I think of you. Or perhaps it''s more of a somersault.',
        99,
        8,
        18
    ),
    (
        'For a few reasons each cherish tune makes me think of you….',
        57,
        0,
        110
    ),
    (
        'God made the world in six days, rested on the seventh, but it took him thousands of a long time to create somebody as flawless as you.',
        172,
        10,
        205
    ),
    (
        'I figure your guardians are bakers because they made you such a cutie pie!',
        73,
        4,
        125
    ),
    (
        'You looked so excellent the final time I saw you, that I overlooked what I was attending to say.',
        146,
        1,
        179
    ),
    (
        'I can''t raise the boldness to tell you how much I worship you, so I guess I''ll just keep it to myself.',
        96,
        0,
        190
    ),
    (
        'I''m so bored at work, come and rescue me.',
        27,
        3,
        65
    ),
    (
        'You looked incredible nowadays. I know I didn''t see you, but I know you look beautiful every day.',
        93,
        2,
        52
    ),
    (
        'Send me a photo, so that I can give my wish list to Santa.',
        104,
        0,
        107
    ),
    (
        'The sunflowers would have gone into the garbage if Van Gogh had you as a subject.',
        107,
        3,
        188
    ),
    (
        'You can''t be replaced.',
        171,
        10,
        71
    ),
    (
        'This picture looks stunning, where did you get it from?',
        12,
        3,
        79
    ),
    (
        'Hey, I''m only preparing for my future and I would like to ask you: are you free for the rest of your life?',
        89,
        0,
        168
    );