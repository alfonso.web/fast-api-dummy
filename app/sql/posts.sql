INSERT INTO
    "post" (
        "title",
        "body",
        "tags",
        "likes",
        "dislikes",
        "views",
        "user_id"
    )
VALUES (
        'His mother had always taught him',
        'His mother had always taught him not to ever think of himself as better than others. He''d tried to live by this motto. He never looked down on those who were less fortunate or who had less money than him. But the stupidity of the group of people he was talking to made him change his mind.',
        ARRAY[
            'history',
            'american',
            'crime'
        ],
        192,
        25,
        305,
        121
    ),
    (
        'He was an expert but not in a discipline',
        'He was an expert but not in a discipline that anyone could fully appreciate. He knew how to hold the cone just right so that the soft server ice-cream fell into it at the precise angle to form a perfect cone each and every time. It had taken years to perfect and he could now do it without even putting any thought behind it.',
        ARRAY[
            'french',
            'fiction',
            'english'
        ],
        859,
        32,
        4884,
        91
    ),
    (
        'Dave watched as the forest burned up on the hill.',
        'Dave watched as the forest burned up on the hill, only a few miles from her house. The car had been hastily packed and Marta was inside trying to round up the last of the pets. Dave went through his mental list of the most important papers and documents that they couldn''t leave behind. He scolded himself for not having prepared these better in advance and hoped that he had remembered everything that was needed. He continued to wait for Marta to appear with the pets, but she still was nowhere to be seen.',
        ARRAY[
            'magical',
            'history',
            'french'
        ],
        1448,
        39,
        4152,
        16
    ),
    (
        'All he wanted was a candy bar.',
        'All he wanted was a candy bar. It didn''t seem like a difficult request to comprehend, but the clerk remained frozen and didn''t seem to want to honor the request. It might have had something to do with the gun pointed at his face.',
        ARRAY[
            'mystery',
            'english',
            'american'
        ],
        359,
        18,
        4548,
        47
    ),
    (
        'Hopes and dreams were dashed that day.',
        'Hopes and dreams were dashed that day. It should have been expected, but it still came as a shock. The warning signs had been ignored in favor of the possibility, however remote, that it could actually happen. That possibility had grown from hope to an undeniable belief it must be destiny. That was until it wasn''t and the hopes and dreams came crashing down.',
        ARRAY['crime', 'mystery', 'love'],
        119,
        30,
        626,
        131
    ),
    (
        'Dave wasn''t exactly sure how he had ended up',
        'Dave wasn''t exactly sure how he had ended up in this predicament. He ran through all the events that had lead to this current situation and it still didn''t make sense. He wanted to spend some time to try and make sense of it all, but he had higher priorities at the moment. The first was how to get out of his current situation of being naked in a tree with snow falling all around and no way for him to get down.',
        ARRAY[
            'english',
            'classic',
            'american'
        ],
        15,
        8,
        38,
        98
    ),
    (
        'This is important to remember.',
        'This is important to remember. Love isn''t like pie. You don''t need to divide it among all your friends and loved ones. No matter how much love you give, you can always give more. It doesn''t run out, so don''t try to hold back giving it as if it may one day run out. Give it freely and as much as you want.',
        ARRAY['magical', 'crime'],
        127,
        26,
        168,
        70
    ),
    (
        'One can cook on and with an open fire.',
        'One can cook on and with an open fire. These are some of the ways to cook with fire outside. Cooking meat using a spit is a great way to evenly cook meat. In order to keep meat from burning, it''s best to slowly rotate it.',
        ARRAY['american', 'english'],
        1271,
        36,
        2116,
        67
    ),
    (
        'There are different types of secrets.',
        'There are different types of secrets. She had held onto plenty of them during her life, but this one was different. She found herself holding onto the worst type. It was the type of secret that could gnaw away at your insides if you didn''t tell someone about it, but it could end up getting you killed if you did.',
        ARRAY[
            'american',
            'history',
            'magical'
        ],
        703,
        18,
        2235,
        82
    ),
    (
        'They rushed out the door.',
        'They rushed out the door, grabbing anything and everything they could think of they might need. There was no time to double-check to make sure they weren''t leaving something important behind. Everything was thrown into the car and they sped off. Thirty minutes later they were safe and that was when it dawned on them that they had forgotten the most important thing of all.',
        ARRAY[
            'fiction',
            'magical',
            'history'
        ],
        455,
        3,
        4504,
        144
    ),
    (
        'It wasn''t quite yet time to panic.',
        'It wasn''t quite yet time to panic. There was still time to salvage the situation. At least that is what she was telling himself. The reality was that it was time to panic and there wasn''t time to salvage the situation, but he continued to delude himself into believing there was.',
        ARRAY[
            'mystery',
            'american',
            'history'
        ],
        453,
        8,
        984,
        43
    ),
    (
        'She was aware that things could go wrong.',
        'She was aware that things could go wrong. In fact, she had trained her entire life in anticipation that things would go wrong one day. She had quiet confidence as she started to see that this was the day that all her training would be worthwhile and useful. At this point, she had no idea just how wrong everything would go that day.',
        ARRAY['love', 'english'],
        362,
        45,
        480,
        82
    ),
    (
        'She wanted rainbow hair.',
        'She wanted rainbow hair. That''s what she told the hairdresser. It should be deep rainbow colors, too. She wasn''t interested in pastel rainbow hair. She wanted it deep and vibrant so there was no doubt that she had done this on purpose.',
        ARRAY[
            'mystery',
            'classic',
            'french'
        ],
        268,
        39,
        3038,
        199
    ),
    (
        'The paper was blank.',
        'The paper was blank. It shouldn''t have been. There should have been writing on the paper, at least a paragraph if not more. The fact that the writing wasn''t there was frustrating. Actually, it was even more than frustrating. It was downright distressing.',
        ARRAY['mystery', 'english', 'love'],
        118,
        17,
        721,
        140
    ),
    (
        'The trees, therefore, must be such old',
        'The trees, therefore, must be such old and primitive techniques that they thought nothing of them, deeming them so inconsequential that even savages like us would know of them and not be suspicious. At that, they probably didn''t have too much time after they detected us orbiting and intending to land. And if that were true, there could be only one place where their civilization was hidden.',
        ARRAY['fiction', 'history', 'crime'],
        951,
        12,
        2911,
        1
    ),
    (
        'There was only one way to do things in the Statton house.',
        'There was only one way to do things in the Statton house. That one way was to do exactly what the father, Charlie, demanded. He made the decisions and everyone else followed without question. That was until today.',
        ARRAY[
            'magical',
            'french',
            'american'
        ],
        1257,
        11,
        2019,
        99
    ),
    (
        'She was in a hurry.',
        'She was in a hurry. Not the standard hurry when you''re in a rush to get someplace, but a frantic hurry. The type of hurry where a few seconds could mean life or death. She raced down the road ignoring speed limits and weaving between cars. She was only a few minutes away when traffic came to a dead standstill on the road ahead.',
        ARRAY[
            'french',
            'magical',
            'english'
        ],
        50,
        31,
        511,
        30
    ),
    (
        'She had a terrible habit o comparing her life to others',
        'She had a terrible habit o comparing her life to others. She realized that their life experiences were completely different than her own and that she saw only what they wanted her to see, but that didn''t matter. She still compared herself and yearned for what she thought they had and she didn''t.',
        ARRAY['history', 'french', 'love'],
        277,
        29,
        1127,
        97
    ),
    (
        'The rain and wind abruptly stopped.',
        'The rain and wind abruptly stopped, but the sky still had the gray swirls of storms in the distance. Dave knew this feeling all too well. The calm before the storm. He only had a limited amount of time before all Hell broke loose, but he stopped to admire the calmness. Maybe it would be different this time, he thought, with the knowledge deep within that it wouldn''t.',
        ARRAY['fiction', 'crime', 'magical'],
        566,
        2,
        2128,
        143
    ),
    (
        'He couldn''t remember exactly where he had read it',
        'He couldn''t remember exactly where he had read it, but he was sure that he had. The fact that she didn''t believe him was quite frustrating as he began to search the Internet to find the article. It wasn''t as if it was something that seemed impossible. Yet she insisted on always seeing the source whenever he stated a fact.',
        ARRAY['french', 'classic'],
        999,
        29,
        4280,
        12
    ),
    (
        'He wandered down the stairs and into the basement',
        'He wandered down the stairs and into the basement. The damp, musty smell of un-use hung in the air. A single, small window let in a glimmer of light, but this simply made the shadows in the basement deeper. He inhaled deeply and looked around at a mess that had been accumulating for over 25 years. He was positive that this was the place he wanted to live.',
        ARRAY['french', 'american'],
        1376,
        13,
        2738,
        136
    ),
    (
        'She has seen this scene before.',
        'She has seen this scene before. It had come to her in dreams many times before. She had to pinch herself to make sure it wasn''t a dream again. As her fingers squeezed against her arm, she felt the pain. It was this pain that immediately woke her up.',
        ARRAY['classic', 'love', 'history'],
        1241,
        20,
        4587,
        183
    ),
    (
        'It''s an unfortunate reality that we don''t teach people how to make money',
        'It''s an unfortunate reality that we don''t teach people how to make money (beyond getting a 9 to 5 job) as part of our education system. The truth is there are a lot of different, legitimate ways to make money. That doesn''t mean they are easy and that you won''t have to work hard to succeed, but it does mean that if you''re willing to open your mind a bit you don''t have to be stuck in an office from 9 to 5 for the next fifty years o your life.',
        ARRAY['crime', 'english'],
        675,
        16,
        1634,
        206
    ),
    (
        'The robot clicked disapprovingly.',
        'The robot clicked disapprovingly, gurgled briefly inside its cubical interior and extruded a pony glass of brownish liquid. Sir, you will undoubtedly end up in a drunkard''s grave, dead of hepatic cirrhosis, it informed me virtuously as it returned my ID card. I glared as I pushed the glass across the table.',
        ARRAY['crime', 'american', 'love'],
        87,
        12,
        97,
        124
    ),
    (
        'It went through such rapid contortions',
        'It went through such rapid contortions that the little bear was forced to change his hold on it so many times he became confused in the darkness, and could not, for the life of him, tell whether he held the sheep right side up, or upside down. But that point was decided for him a moment later by the animal itself, who, with a sudden twist, jabbed its horns so hard into his lowest ribs that he gave a grunt of anger and disgust.',
        ARRAY[
            'fiction',
            'history',
            'french'
        ],
        1162,
        16,
        2437,
        148
    ),
    (
        'She patiently waited for his number to be called.',
        'She patiently waited for his number to be called. She had no desire to be there, but her mom had insisted that she go. She''s resisted at first, but over time she realized it was simply easier to appease her and go. Mom tended to be that way. She would keep insisting until you wore down and did what she wanted. So, here she sat, patiently waiting for her number to be called.',
        ARRAY['french', 'mystery', 'crime'],
        444,
        3,
        4070,
        156
    ),
    (
        'Ten more steps.',
        'If he could take ten more steps it would be over, but his legs wouldn''t move. He tried to will them to work, but they wouldn''t listen to his brain. Ten more steps and it would be over but it didn''t appear he would be able to do it.',
        ARRAY['mystery', 'classic', 'love'],
        1387,
        35,
        3218,
        95
    ),
    (
        'He had three simple rules by which he lived.',
        'He had three simple rules by which he lived. The first was to never eat blue food. There was nothing in nature that was edible that was blue. People often asked about blueberries, but everyone knows those are actually purple. He understood it was one of the stranger rules to live by, but it had served him well thus far in the 50+ years of his life.',
        ARRAY['crime', 'love'],
        1291,
        18,
        3531,
        19
    ),
    (
        'The chair sat in the corner where it had been',
        'The chair sat in the corner where it had been for over 25 years. The only difference was there was someone actually sitting in it. How long had it been since someone had done that? Ten years or more he imagined. Yet there was no denying the presence in the chair now.',
        ARRAY['mystery', 'american'],
        1,
        1,
        43,
        74
    ),
    (
        'Things aren''t going well at all',
        'Things aren''t going well at all with mom today. She is just a limp noodle and wants to sleep all the time. I sure hope that things get better soon.',
        ARRAY['american', 'love', 'fiction'],
        465,
        30,
        3558,
        177
    ),
    (
        'It was just a burger.',
        'It was just a burger. Why couldn''t she understand that? She knew he''d completely changed his life around her eating habits, so why couldn''t she give him a break this one time? She wasn''t even supposed to have found out. Yes, he had promised her and yes, he had broken that promise, but still in his mind, all it had been was just a burger.',
        ARRAY[
            'classic',
            'fiction',
            'magical'
        ],
        443,
        21,
        1656,
        168
    ),
    (
        'He swung back the fishing pole and cast the line',
        'He swung back the fishing pole and cast the line which ell 25 feet away into the river. The lure landed in the perfect spot and he was sure he would soon get a bite. He never expected that the bite would come from behind in the form of a bear.',
        ARRAY[
            'mystery',
            'american',
            'english'
        ],
        227,
        3,
        818,
        6
    ),
    (
        'He lifted the bottle to his lips and took a sip',
        'He lifted the bottle to his lips and took a sip of the drink. He had tasted this before, but he couldn''t quite remember the time and place it had happened. He desperately searched his mind trying to locate and remember where he had tasted this when the bicycle ran over his foot.',
        ARRAY['american', 'french', 'love'],
        40,
        35,
        2608,
        58
    ),
    (
        'Debbie had taken George for granted',
        'Debbie had taken George for granted for more than fifteen years now. He wasn''t sure what exactly had made him choose this time and place to address the issue, but he decided that now was the time. He looked straight into her eyes and just as she was about to speak, turned away and walked out the door.',
        ARRAY[
            'american',
            'crime',
            'history'
        ],
        654,
        15,
        1194,
        98
    ),
    (
        'She sat deep in thought.',
        'She sat deep in thought. The next word that came out o her mouth would likely be the most important word of her life. It had to be exact with no possibility of being misinterpreted. She was ready. She looked deeply into his eyes and said, Octopus.',
        ARRAY[
            'french',
            'american',
            'fiction'
        ],
        273,
        27,
        1312,
        190
    ),
    (
        'The leather jacked showed the scars',
        'The leather jacked showed the scars of being his favorite for years. It wore those scars with pride, feeling that they enhanced his presence rather than diminishing it. The scars gave it character and had not overwhelmed to the point that it had become ratty. The jacket was in its prime and it knew it.',
        ARRAY['french', 'fiction'],
        428,
        19,
        765,
        207
    ),
    (
        'There was no time.',
        'There was no time. He ran out of the door without half the stuff he needed for work, but it didn''t matter. He was late and if he didn''t make this meeting on time, someone''s life may be in danger.',
        ARRAY[
            'history',
            'fiction',
            'english'
        ],
        4,
        1,
        11,
        150
    ),
    (
        'He collected the plastic trash on a daily basis.',
        'He collected the plastic trash on a daily basis. It never seemed to end. Even if he cleaned the entire beach, more plastic would cover it the next day after the tide had come in. Although it was a futile effort that would never be done, he continued to pick up the trash each day.',
        ARRAY[
            'english',
            'mystery',
            'classic'
        ],
        834,
        47,
        2013,
        177
    ),
    (
        'It was so great to hear from you today',
        'It was so great to hear from you today and it was such weird timing, he said. This is going to sound funny and a little strange, but you were in a dream I had just a couple of days ago. I''d love to get together and tell you about it if you''re up for a cup of coffee, he continued, laying the trap he''d been planning for years.',
        ARRAY[
            'french',
            'magical',
            'american'
        ],
        772,
        39,
        1129,
        115
    ),
    (
        'Have you ever wondered about toes?',
        'Why 10 toes and not 12. Why are some bigger than others? Some people can use their toes to pick up things while others can barely move them on command. Some toes are nice to look at while others are definitely not something you want to look at. Toes can be stubbed and make us scream. Toes help us balance and walk. 10 toes are just something to ponder.',
        ARRAY['history', 'love', 'crime'],
        172,
        19,
        4923,
        181
    ),
    (
        'His parents continued to question him.',
        'He didn''t know what to say to them since they refused to believe the truth. He explained again and again, and they dismissed his explanation as a figment of his imagination. There was no way that grandpa, who had been dead for five years, could have told him where the treasure had been hidden. Of course, it didn''t help that grandpa was roaring with laughter in the chair next to him as he tried to explain once again how he''d found it.',
        ARRAY[
            'english',
            'history',
            'american'
        ],
        1211,
        21,
        4476,
        140
    ),
    (
        'You know that tingly feeling you get on the back of your neck sometimes?',
        'You know that tingly feeling you get on the back of your neck sometimes? I just got that feeling when talking with her. You know I don''t believe in sixth senses, but there is something not right with her. I don''t know how I know, but I just do.',
        ARRAY[
            'mystery',
            'french',
            'american'
        ],
        177,
        11,
        3757,
        188
    ),
    (
        'Explain to me again why I shouldn''t cheat?',
        'Explain to me again why I shouldn''t cheat? he asked. All the others do and nobody ever gets punished for doing so. I should go about being happy losing to cheaters because I know that I don''t? That''s what you''re telling me?',
        ARRAY[
            'classic',
            'french',
            'fiction'
        ],
        1338,
        15,
        3359,
        6
    ),
    (
        'A long black shadow slid across the pavement',
        'A long black shadow slid across the pavement near their feet and the five Venusians, very much startled, looked overhead. They were barely in time to see the huge gray form of the carnivore before it vanished behind a sign atop a nearby building which bore the mystifying information Pepsi-Cola.',
        ARRAY['crime', 'english', 'classic'],
        1469,
        28,
        2764,
        124
    ),
    (
        'The red line moved across the page.',
        'The red line moved across the page. With each millimeter it advanced forward, something changed in the room. The actual change taking place was difficult to perceive, but the change was real. The red line continued relentlessly across the page and the room would never be the same.',
        ARRAY['love', 'american'],
        980,
        43,
        2755,
        45
    ),
    (
        'The clowns had taken over. And yes, they were literally clowns.',
        'The clowns had taken over. And yes, they were literally clowns. Over 100 had appeared out of a small VW bug that had been driven up to the bank. Now they were all inside and had taken it over.',
        ARRAY[
            'crime',
            'magical',
            'american'
        ],
        597,
        23,
        3730,
        132
    ),
    (
        'The shoes had been there for as long as anyone could remember.',
        'The shoes had been there for as long as anyone could remember. In fact, it was difficult for anyone to come up with a date they had first appeared. It had seemed they''d always been there and yet they seemed so out of place. Why nobody had removed them was a question that had been asked time and again, but while they all thought it, nobody had ever found the energy to actually do it.',
        ARRAY['american', 'french', 'crime'],
        1007,
        19,
        1176,
        126
    ),
    (
        'Trees. It was something about the trees.',
        'Trees. It was something about the trees. The way they swayed with the wind in unison. The way they shaded the area around them. The sounds of their leaves in the wind and the creaks from the branches as they sway, The trees were making a statement that I just couldn''t understand.',
        ARRAY['love', 'fiction', 'crime'],
        570,
        30,
        4470,
        175
    ),
    (
        'Sometimes it''s just better not to be seen.',
        'Sometimes it''s just better not to be seen. That''s how Harry had always lived his life. He prided himself as being the fly on the wall and the fae that blended into the crowd. That''s why he was so shocked that she noticed him.',
        ARRAY['french', 'classic'],
        390,
        25,
        2928,
        207
    ),
    (
        'It was a concerning development that he couldn''t get out of his mind.',
        'He''d had many friends throughout his early years and had fond memories of playing with them, but he couldn''t understand how it had all stopped. There was some point as he grew up that he played with each of his friends for the very last time, and he had no idea that it would be the last.',
        ARRAY['french', 'fiction', 'love'],
        1136,
        49,
        1455,
        15
    ),
    (
        'The towels had been hanging from the rod for years.',
        'The towels had been hanging from the rod for years. They were stained and worn, and quite frankly, just plain ugly. Debra didn''t want to touch them but she really didn''t have a choice. It was important for her to see what was living within them.',
        ARRAY[
            'magical',
            'english',
            'american'
        ],
        711,
        46,
        4384,
        69
    ),
    (
        'The headache wouldn''t go away.',
        'The headache wouldn''t go away. She''s taken medicine but even that didn''t help. The monstrous throbbing in her head continued. She had this happen to her only once before in her life and she realized that only one thing could be happening.',
        ARRAY['mystery', 'love'],
        524,
        42,
        2569,
        135
    ),
    (
        'The young man wanted a role model.',
        'The young man wanted a role model. He looked long and hard in his youth, but that role model never materialized. His only choice was to embrace all the people in his life he didn''t want to be like.',
        ARRAY['magical', 'american'],
        1157,
        5,
        1341,
        48
    ),
    (
        'Debbie knew she was being selfish',
        'Debbie knew she was being selfish and unreasonable. She understood why the others in the room were angry and frustrated with her and the way she was acting. In her eyes, it didn''t really matter how they felt because she simply didn''t care.',
        ARRAY['english', 'american'],
        393,
        14,
        4364,
        200
    ),
    (
        'She tried to explain that love wasn''t like pie.',
        'She tried to explain that love wasn''t like pie. There wasn''t a set number of slices to be given out. There wasn''t less to be given to one person if you wanted to give more to another. That after a set amount was given out it would all disappear. She tried to explain this, but it fell on deaf ears.',
        ARRAY['mystery', 'crime', 'fiction'],
        56,
        46,
        380,
        105
    ),
    (
        'The house was located at the top of the hill',
        'The house was located at the top of the hill at the end of a winding road. It wasn''t obvious that the house was there, but everyone in town knew that it existed. They were just all too afraid to ever go and see it in person.',
        ARRAY[
            'classic',
            'french',
            'history'
        ],
        738,
        11,
        4728,
        191
    ),
    (
        'It seemed like it should have been so simple.',
        'It seemed like it should have been so simple. There was nothing inherently difficult with getting the project done. It was simple and straightforward enough that even a child should have been able to complete it on time, but that wasn''t the case. The deadline had arrived and the project remained unfinished.',
        ARRAY['love', 'classic', 'english'],
        66,
        30,
        469,
        72
    ),
    (
        'Balloons are pretty and come in different colors',
        'Balloons are pretty and come in different colors, different shapes, different sizes, and they can even adjust sizes as needed. But don''t make them too big or they might just pop, and then bye-bye balloon. It''ll be gone and lost for the rest of mankind. They can serve a variety of purposes, from decorating to water balloon wars. You just have to use your head to think a little bit about what to do with them.',
        ARRAY[
            'american',
            'crime',
            'magical'
        ],
        127,
        45,
        250,
        93
    ),
    (
        'She looked at her student wondering if she could ever get through.',
        'She looked at her student wondering if she could ever get through. You need to learn to think for yourself, she wanted to tell him. Your friends are holding you back and bringing you down. But she didn''t because she knew his friends were all that he had and even if that meant a life of misery, he would never give them up.',
        ARRAY[
            'classic',
            'english',
            'history'
        ],
        475,
        13,
        4816,
        89
    ),
    (
        'He heard the crack echo in the late afternoon about a mile away.',
        'He heard the crack echo in the late afternoon about a mile away. His heart started racing and he bolted into a full sprint. It wasn''t a gunshot, it wasn''t a gunshot, he repeated under his breathlessness as he continued to sprint.',
        ARRAY['love', 'classic', 'history'],
        1351,
        41,
        2699,
        204
    ),
    (
        'I''m going to hire professional help tomorrow.',
        'I''m going to hire professional help tomorrow. I can''t handle this anymore. She fell over the coffee table and now there is blood in her catheter. This is much more than I ever signed up to do.',
        ARRAY[
            'fiction',
            'classic',
            'american'
        ],
        1127,
        40,
        4419,
        5
    ),
    (
        'He watched as the young man tried',
        'He watched as the young man tried to impress everyone in the room with his intelligence. There was no doubt that he was smart. The fact that he was more intelligent than anyone else in the room could have been easily deduced, but nobody was really paying any attention due to the fact that it was also obvious that the young man only cared about his intelligence.',
        ARRAY['magical', 'mystery', 'love'],
        553,
        9,
        1880,
        55
    ),
    (
        'Many people say that life isn''t like a bed of roses.',
        'Many people say that life isn''t like a bed of roses. I beg to differ. I think that life is quite like a bed of roses. Just like life, a bed of roses looks pretty on the outside, but when you''re in it, you find that it is nothing but thorns and pain. I myself have been pricked quite badly.',
        ARRAY['magical'],
        36,
        10,
        136,
        132
    ),
    (
        'There are only three ways to make this work.',
        'There are only three ways to make this work. The first is to let me take care of everything. The second is for you to take care of everything. The third is to split everything 50 / 50. I think the last option is the most preferable, but I''m certain it''ll also mean the end of our marriage.',
        ARRAY['classic', 'english'],
        224,
        11,
        1418,
        170
    ),
    (
        'Time is all relative based on age and experience.',
        'As a middle-aged adult, time flies by as you watch your children grow up. And finally, as you get old and you have fewer responsibilities and fewer demands on you, time slows. You appreciate each day and are thankful you are alive. An hour is the same amount of time for everyone yet it can feel so different in how it goes by.',
        ARRAY['love', 'fiction', 'classic'],
        538,
        10,
        4830,
        76
    ),
    (
        'Time is all relative based on age and experience.',
        'When you are a child an hour is a long time to wait but a very short time when that''s all the time you are allowed on your iPad. As a teenager time goes faster the more deadlines you have and the more you procrastinate. As a young adult, you think you have forever to live and don''t appreciate the time you spend with others.',
        ARRAY['mystery', 'magical', 'crime'],
        804,
        15,
        4907,
        124
    ),
    (
        'Welcome to my world.',
        'You will be greeted by the unexpected here and your mind will be challenged and expanded in ways that you never thought possible. That is if you are able to survive...',
        ARRAY['love', 'history'],
        651,
        15,
        3326,
        173
    ),
    (
        'She sat down with her notebook in her hand',
        'She sat down with her notebook in her hand, her mind wandering to faraway places. She paused and considered all that had happened. It hadn''t gone as expected. When the day began she thought it was going to be a bad one, but as she sat recalling the day''s events to write them down, she had to admit, it had been a rather marvelous day.',
        ARRAY['history', 'english', 'love'],
        372,
        16,
        3551,
        70
    ),
    (
        'The wave roared towards them with speed and violence they had not anticipated.',
        'The wave roared towards them with speed and violence they had not anticipated. They both turned to run but by that time it was too late. The wave crashed into their legs sweeping both of them off of their feet. They now found themselves in a washing machine of saltwater, getting tumbled and not know what was up or down.',
        ARRAY[
            'american',
            'fiction',
            'classic'
        ],
        497,
        39,
        1758,
        196
    ),
    (
        'Sometimes there isn''t a good answer.',
        'Sometimes there isn''t a good answer. No matter how you try to rationalize the outcome, it doesn''t make sense. And instead of an answer, you are simply left with a question. Why?',
        ARRAY['english', 'history', 'crime'],
        1145,
        45,
        2060,
        101
    ),
    (
        'He knew what he was supposed to do.',
        'He knew what he was supposed to do. That had been apparent from the beginning. That was what made the choice so difficult. What he was supposed to do and what he would do were not the same. This would have been fine if he were willing to face the inevitable consequences, but he wasn''t.',
        ARRAY['love', 'fiction'],
        676,
        22,
        1765,
        203
    ),
    (
        'The words hadn''t flowed from his fingers',
        'The words hadn''t flowed from his fingers for the past few weeks. He never imagined he''d find himself with writer''s block, but here he sat with a blank screen in front of him.',
        ARRAY['crime', 'history', 'classic'],
        681,
        44,
        4577,
        112
    ),
    (
        'It was difficult to explain to them',
        'It was difficult to explain to them how the diagnosis of certain death had actually given him life. While everyone around him was in tears and upset, he actually felt more at ease. The doctor said it would be less than a year. That gave him a year to live, something he''d failed to do with his daily drudgery of a routine that had passed as life until then.',
        ARRAY[
            'fiction',
            'classic',
            'magical'
        ],
        389,
        37,
        3462,
        155
    ),
    (
        'He couldn''t move. His head throbbed and spun.',
        'He couldn''t move. His head throbbed and spun. He couldn''t decide if it was the flu or the drinking last night. It was probably a combination of both.',
        ARRAY['crime', 'fiction', 'classic'],
        450,
        15,
        2104,
        152
    ),
    (
        'There was something beautiful in his hate.',
        'There was something beautiful in his hate. It wasn''t the hate itself as it was a disgusting display of racism and intolerance. It was what propelled the hate and the fact that although he had this hate, he didn''t understand where it came from. It was at that moment that she realized that there was hope in changing him.',
        ARRAY['classic', 'love', 'french'],
        125,
        31,
        220,
        54
    ),
    (
        'Her mom had warned her.',
        'Her mom had warned her. She had been warned time and again, but she had refused to believe her. She had done everything right and she knew she would be rewarded for doing so with the promotion.',
        ARRAY['mystery', 'english'],
        552,
        36,
        657,
        13
    ),
    (
        'She nervously peered over the edge.',
        'She nervously peered over the edge. She understood in her mind that the view was supposed to be beautiful, but all she felt was fear. There had always been something about heights that disturbed her, and now she could feel the full force of this unease.',
        ARRAY['crime', 'english', 'french'],
        327,
        42,
        952,
        114
    ),
    (
        'The thing that''s great about this job',
        'The thing that''s great about this job is the time sourcing the items involves no traveling. I just look online to buy it. It''s really as simple as that. While everyone else is searching for what they can sell, I sit in front of my computer and buy better stuff for less money and spend a fraction of the time doing it.',
        ARRAY['crime', 'magical'],
        37,
        4,
        4020,
        51
    ),
    (
        'It was a simple tip of the hat',
        'Grace didn''t think that anyone else besides her had even noticed it. It wasn''t anything that the average person would notice, let alone remember at the end of the day.',
        ARRAY[
            'american',
            'classic',
            'history'
        ],
        1319,
        28,
        3554,
        45
    ),
    (
        'Cake or pie?',
        'I can tell a lot about you by which one you pick. It may seem silly, but cake people and pie people are really different. I know which one I hope you are, but that''s not for me to decide. So, what is it? Cake or pie?',
        ARRAY['classic', 'love', 'french'],
        1007,
        22,
        2670,
        80
    ),
    (
        'There was something in the tree.',
        'There was something in the tree. It was difficult to tell from the ground, but Rachael could see movement. She squinted her eyes and peered in the direction of the movement, trying to decipher exactly what she had spied.',
        ARRAY['mystery', 'fiction', 'love'],
        550,
        45,
        4589,
        126
    ),
    (
        'Pink ponies and purple giraffes roamed the field.',
        'Pink ponies and purple giraffes roamed the field. Cotton candy grew from the ground as a chocolate river meandered off to the side. What looked like stones in the pasture were actually rock candy. Everything in her dream seemed to be perfect except for the fact that she had no mouth.',
        ARRAY[
            'french',
            'classic',
            'mystery'
        ],
        915,
        25,
        2424,
        79
    ),
    (
        'Are you getting my texts???',
        'Are you getting my texts??? she texted to him. He glanced at it and chuckled under his breath. Of course he was getting them, but if he wasn''t getting them, how would he ever be able to answer? He put the phone down and continued on his project. He was ignoring her texts and he planned to continue to do so.',
        ARRAY[
            'mystery',
            'magical',
            'history'
        ],
        822,
        14,
        837,
        84
    ),
    (
        'He stepped away from the mic. This was the best take he had done so far',
        'He stepped away from the mic. This was the best take he had done so far, but something seemed missing. Then it struck him all at once. Visuals ran in front of his eyes and music rang in his ears. His eager fingers went to work in an attempt to capture his thoughts hoping the results would produce something that was at least half their glory.',
        ARRAY['magical', 'fiction'],
        383,
        4,
        511,
        145
    ),
    (
        'The choice was red, green, or blue.',
        'It didn''t seem like an important choice when he was making it, but it was a choice nonetheless. Had he known the consequences at that time, he would likely have considered the choice a bit longer. In the end, he didn''t and ended up choosing blue.',
        ARRAY['english', 'fiction'],
        395,
        45,
        2285,
        47
    ),
    (
        'He picked up the burnt end of the branch and made a mark on the stone.',
        'Day 52 if the marks on the stone were accurate. He couldn''t be sure. Day and nights had begun to blend together creating confusion, but he knew it was a long time. Much too long.',
        ARRAY['english', 'love', 'fiction'],
        834,
        23,
        2761,
        171
    ),
    (
        'The red glint of paint sparkled under the sun.',
        'He had dreamed of owning this car since he was ten, and that dream had become a reality less than a year ago. It was his baby and he spent hours caring for it, pampering it, and fondling over it. She knew this all too well, and that''s exactly why she had taken a sludge hammer to it.',
        ARRAY['english', 'love', 'french'],
        251,
        8,
        390,
        144
    ),
    (
        'There were little things that she simply could not stand.',
        'There were little things that she simply could not stand. The sound of someone tapping their nails on the table. A person chewing with their mouth open. Another human imposing themselves into her space. She couldn''t stand any of these things, but none of them compared to the number one thing she couldn''t stand which topped all of them combined.',
        ARRAY[
            'history',
            'classic',
            'magical'
        ],
        1136,
        18,
        1322,
        51
    ),
    (
        'On Saturday nights I would sit by the phone',
        'in the lobby, waiting for Naoko to call. Most of the others were out, so the lobby was usually deserted. I would stare at the grains of light suspended in that silent space, struggling to see into my own heart. What did I want? ',
        ARRAY['english', 'mystery'],
        58,
        37,
        2817,
        104
    ),
    (
        'Gentlemen of the free-and-easy sort',
        'plume themselves on being acquainted with a move or two, and being usually equal to the time-of-day, express the wide range of their capacity for adventure by observing that they are good for anything from pitch-and-toss to manslaughter;',
        ARRAY['love', 'history'],
        1211,
        24,
        3271,
        150
    ),
    (
        'In truth, Mrs. Gradgrind''s stock of facts in',
        'general was woefully defective; but Mr. Gradgrind in raising her to her high matrimonial position, had been influenced by two reasons.  Firstly, she was most satisfactory as a question of figures; and, secondly,',
        ARRAY['love', 'crime'],
        115,
        32,
        512,
        204
    ),
    (
        '''My dear Bounderby,'' Mr. Gradgrind began',
        '''Now, you''ll excuse me,'' said Bounderby, ''but I don''t want to be too dear.  That, to start with.  When I begin to be dear to a man, I generally find that his intention is to come over me.',
        ARRAY['classic', 'love', 'english'],
        191,
        37,
        3533,
        102
    ),
    (
        'He could find no answer, except life''s usual',
        'answer to the most complex and insoluble questions. That answer is: live in the needs of the day, that is, find forgetfulness.',
        ARRAY[
            'magical',
            'american',
            'classic'
        ],
        75,
        3,
        828,
        169
    ),
    (
        'Happiness was different in childhood.',
        'so much then a matter simply of accumulation, of taking things - new experiences, new emotions - and applying them like so many polished tiles to what would someday be the marvellously finished pavilion of the self.',
        ARRAY[
            'american',
            'crime',
            'mystery'
        ],
        573,
        24,
        2319,
        28
    ),
    (
        'So what is the answer? How can you stand',
        'From the moment you go to prison you must put your cozy past firmly behind you. At the very threshold, you must say to yourself: “My life is over, a little early to be sure, but there''s nothing to be done about it. I shall never return to freedom.',
        ARRAY['love', 'fiction', 'history'],
        8,
        4,
        8,
        113
    ),
    (
        'If only it were all so simple! If only there',
        'were evil people somewhere insidiously committing evil deeds, and it were necessary only to separate them from the rest of us and destroy them. But the line dividing good and evil cuts through the heart of every human being. And who is willing to destroy a piece of his own heart?',
        ARRAY[
            'classic',
            'mystery',
            'french'
        ],
        64,
        50,
        2095,
        198
    ),
    (
        'As for the leaflets reporting the creation of',
        'the ROA, the “Russian Liberation Army,” not only were they written in bad Russian, but they were imbued with an alien spirit that was clearly German and, moreover, seemed little concerned with their presumed subject; besides, and on the other hand,',
        ARRAY['classic', 'fiction', 'crime'],
        99,
        40,
        154,
        72
    ),
    (
        'And how can you bring it home to them?',
        'an inspiration? By a vision? A dream? Brothers! People! Why has life been given you? In the deep, deaf stillness of midnight, the doors of the death cells are being swung open - and great-souled people are being dragged out to be shot.',
        ARRAY[
            'english',
            'mystery',
            'fiction'
        ],
        652,
        21,
        1158,
        161
    ),
    (
        'Like all men not really up to their job,',
        'a stickler for externals and petty quotidian things; and in lieu of an intellect he had accumulated an armoury of capitalized key-words like Discipline and Tradition and Responsibility.',
        ARRAY['crime', 'classic'],
        1377,
        22,
        3342,
        154
    ),
    (
        'But Art is a punitive sentence, not a',
        'birthright, & there is nothing in my early life that suggests artistic aptitude or even interest, my pastimes & fascinations nearly all being what may - & were - deemed the merely villainous.',
        ARRAY['love', 'history', 'crime'],
        121,
        1,
        252,
        131
    ),
    (
        'All Hallows Day: grief comes in waves.',
        't threatens to capsize him. He doesn''t believe that the dead come back; but that doesn''t stop him from feeling the brush of their fingertips, wingtips, against his shoulder.',
        ARRAY['classic', 'crime', 'english'],
        1341,
        6,
        4695,
        72
    ),
    (
        'Being in high school, Miles had no idea there',
        'were girls in the world who might be nice to some boy who''d suffered the misfortune of falling in love with them, even when they couldn''t return the favor.',
        ARRAY['love', 'american', 'english'],
        1091,
        11,
        1397,
        73
    ),
    (
        'But they didn''t devote the whole evening to',
        'music. After a while they played at forfeits; for it is good to be children sometimes, and never better than at Christmas, when its mighty Founder was a child himself. Stop. There was first a game at blind-man''s buff.',
        ARRAY[
            'fiction',
            'french',
            'classic'
        ],
        1088,
        32,
        4031,
        66
    ),
    (
        'Each failed overture of peace made the next',
        'overture less likely to succeed. Before long, what at first glance had seemed to Gary an absurd possibility - that the till of their marriage no longer contained sufficient funds of love and goodwill to cover the emotional costs that going to St.',
        ARRAY[
            'american',
            'english',
            'crime'
        ],
        1049,
        29,
        4033,
        77
    ),
    (
        'All men dream, but not equally.',
        'dream by night in the dusty recesses of their minds wake in the day to find that it was vanity: but the dreamers of the day are dangerous men, for they may act their dreams with open eyes.',
        ARRAY[
            'french',
            'magical',
            'english'
        ],
        154,
        41,
        827,
        97
    ),
    (
        'Sometimes… Come on, how often exactly,',
        'Bert? Can you recall four, five, more such occasions? Or would no human heart have survived two or three? Sometimes (I have nothing to say in reply to your question), while Lolita would be haphazardly preparing her homework,',
        ARRAY[
            'mystery',
            'english',
            'french'
        ],
        451,
        47,
        1667,
        18
    ),
    (
        'She would never know, because he would',
        'never tell her. Somehow if she''d known the worst parts, she couldn''t have gone on being a haven for him. He was groping for an idea that he couldn''t quite grasp.',
        ARRAY['french', 'american', 'crime'],
        429,
        15,
        861,
        18
    ),
    (
        'He ran as he''d never run before',
        'hope nor despair. He ran because the world was divided into opposites and his side had already been chosen for him, his only choice being whether or not to play his part with heart and courage.',
        ARRAY['magical', 'mystery', 'love'],
        914,
        2,
        3679,
        174
    ),
    (
        'How vulgar, this hankering after',
        'mortality, how vain, how false. Composers are merely scribblers of cave paintings. One writes music because winter is eternal and because, if one didn''t, the wolves and blizzards would be at one''s throat all the sooner.',
        ARRAY[
            'mystery',
            'classic',
            'history'
        ],
        815,
        36,
        2380,
        23
    ),
    (
        'The embassy''s door was of bulletproof steel',
        'lined with a veneer of English oak. You attained it by touching a button in a silent lift. The royal crest, in this air-conditioned stillness, suggested silicone and funeral parlours.',
        ARRAY[
            'classic',
            'magical',
            'american'
        ],
        73,
        33,
        3332,
        107
    ),
    (
        'Act, implores the Ghost of Future Regret.',
        'Jacob hurries past the tomatoes and catches her up near the gate.''Miss Abigawa? Miss Aibagawa. I must ask you to forgive me.''She has turned around and has one hand on the gate. ''Why forgive?',
        ARRAY['crime', 'love'],
        623,
        39,
        1481,
        112
    ),
    (
        'The ship rolls and her timbers creak like',
        'Men of commerce, sir…'' Nash counts out laudanum drops into the pewter beaker ''… for the most part, had their consciences cut out at birth. Better an honest drowning than slow death by hypocrisy,',
        ARRAY[
            'fiction',
            'history',
            'american'
        ],
        432,
        43,
        578,
        32
    ),
    (
        'He let the phone slip from his hand and lay',
        'crying for a while, silently, shaking the cheap bed. He didn''t know what to do, he didn''t know how to live. Each new thing he encountered in life impelled him in a direction that fully convinced him of its rightness,',
        ARRAY['french', 'fiction'],
        926,
        1,
        4698,
        171
    ),
    (
        'When they leave the church, the last light is',
        'vanishing into the sky, and a stray snowflake drifts along towards the south. They remount; it has been a long day; his clothes feel heavy on his back.',
        ARRAY['french', 'classic', 'love'],
        876,
        14,
        2891,
        67
    ),
    (
        'In the days to follow the hacendado would',
        'come up to the corral where they''d shaped the manada and he and John Grady would walk among the mares and John Grady would argue their points and the hacendado would muse and walk away a fixed distance.',
        ARRAY[
            'american',
            'fiction',
            'history'
        ],
        358,
        37,
        2093,
        72
    ),
    (
        'Imagine the silence now, in that place which',
        'is no-place, that anteroom to God where each hour is ten thousand years long. Once you imagined the souls held in a great net, a web spun by God,',
        ARRAY[
            'magical',
            'crime',
            'american'
        ],
        183,
        40,
        1390,
        26
    ),
    (
        'Then, perhaps overcome with nostalgia for',
        'happier times, he gave me a good kicking. Afterwards I assured him he had all the attributes necessary for a successful artistic career, through unfortunately my mouth was too swollen to list them for Pobjoy''s benefit: mediocrity;',
        ARRAY[
            'history',
            'mystery',
            'american'
        ],
        120,
        3,
        3859,
        62
    ),
    (
        'I knew that on the island one was driven',
        'back into the past. There was so much space, so much silence, so few meetings that one too easily saw out of the present, and then the past seemed ten times closer than it was.',
        ARRAY['american', 'magical', 'love'],
        431,
        7,
        1102,
        57
    ),
    (
        'It was but a hurried parting in a common',
        'street, yet it was a sacred remembrance to these two common people.  Utilitarian economists, skeletons of schoolmasters, Commissioners of Fact,',
        ARRAY[
            'history',
            'magical',
            'english'
        ],
        1206,
        0,
        3139,
        13
    ),
    (
        'When a woman withdraws to give birth the',
        'sun may be shining but the shutters of her room are closed so she can make her own weather. She is kept in the dark so she can dream. Her dreams drift her far away,',
        ARRAY['crime', 'history', 'mystery'],
        1311,
        34,
        4914,
        105
    ),
    (
        'Revolution and youth are closely allied.',
        'can a revolution promise to adults? To some it brings disgrace, to others favor. But even that favor is questionable,',
        ARRAY['english', 'history', 'crime'],
        159,
        4,
        4114,
        7
    ),
    (
        'The point was we took this shit very',
        'seriously. They had taken away our flowers, our summer days, and our bonuses, we were on a wage freeze and a hiring freeze, and people were flying out the door like so many dismantled dummies.',
        ARRAY['history', 'english', 'crime'],
        853,
        47,
        3140,
        31
    ),
    (
        'The old scholar was watching the noisy',
        'young people around him and it suddenly occurred to him that he was the only one in the whole audience who had the privilege of freedom, ',
        ARRAY['mystery', 'classic'],
        1350,
        12,
        1583,
        5
    ),
    (
        'Later, on my walk, I wondered why I felt I',
        'had to be suspicious of ''normality''. The striking thing about the normal is that there is nothing normal about it:',
        ARRAY['english', 'mystery'],
        893,
        45,
        974,
        150
    ),
    (
        'Looking back on those incidents,',
        'always appalled by the memory of his passivity, hard though it was to see what else he could have done. He could have refused to pay for the gravy damage to his room, could have refused to change his shoes,',
        ARRAY['fiction', 'classic'],
        1392,
        13,
        4240,
        152
    ),
    (
        'I hoped she did not dislike me,',
        'under no illusions that she might remember me in any way fondly; that is, if she remembered me at all. I was but one of a procession; I provided extra food, drink, that day some tobacco, beyond that I did not exist for her.',
        ARRAY[
            'english',
            'classic',
            'mystery'
        ],
        120,
        43,
        278,
        102
    ),
    (
        'If I may so express it, I was steeped in Dora.',
        'was not merely over head and ears in love with her, but I was saturated through and through. Enough love might have been wrung out of me, metaphorically speaking,',
        ARRAY['english', 'american'],
        400,
        21,
        767,
        43
    ),
    (
        'Lyrical poetry is a realm in which any',
        'statement immediately becomes truth. Yesterday the poet said life is a vale of tears; today he said life is a land of smiles; and he was right both times. There is no inconsistency.',
        ARRAY['crime', 'love', 'english'],
        39,
        4,
        2379,
        63
    ),
    (
        'Sometimes, when Chapuys has finished',
        'digging up Walter''s bones and making his own life unfamiliar to him, he feels almost impelled to speak in defense of his father, his childhood. But it is no use to justify yourself. It is no good to explain. It is weak to be anecdotal.',
        ARRAY['american', 'love', 'magical'],
        63,
        10,
        4850,
        128
    ),
    (
        'Always I had acted as if a third person was watching',
        'Always I had acted as if a third person was watching and listening and giving me marks for good or bad behaviour - a god like a novelist, to whom I turned, like a character with the power to please, the sensitivity to feel slighted, the ability to adapt himself to whatever he believed the novelist-god wanted.',
        ARRAY[
            'english',
            'crime',
            'american'
        ],
        985,
        36,
        4261,
        48
    ),
    (
        'What was I after all?',
        'Near enough what Conchis had had me told: nothing but the net sum of countless wrong turnings. I dismissed most of the Freudian jargon of the trial; but all my life I had tried to turn life to fiction, to hold reality away.',
        ARRAY['crime', 'english'],
        1455,
        43,
        2810,
        30
    ),
    (
        'Now they were in the earth',
        'Now they were in the earth to which they had given their lives; and slowly, year by year, the earth would take them.',
        ARRAY['french', 'american'],
        120,
        9,
        687,
        170
    ),
    (
        'He thought of the cost exacted',
        'He thought of the cost exacted, year after year, by the soil; and it remained as it had been—a little more barren, perhaps, a little more frugal of increase. Nothing had changed. Their lives had been expended in cheerless labor, their wills broken, their intelligences numbed.',
        ARRAY['classic', 'history', 'crime'],
        1028,
        28,
        1248,
        116
    ),
    (
        'He buried her beside her husband',
        'He buried her beside her husband. After the services were over and the few mourners had gone, he stood alone in a cold November wind and looked at the two graves, one open to its burden and the other mounded and covered by a thin fuzz of grass.',
        ARRAY['mystery', 'english'],
        15,
        6,
        4437,
        97
    ),
    (
        'I was disconcerted, for I had broken away',
        'I was disconcerted, for I had broken away without quite seeing where I was going to. It was not to be shuffled off now, however, and I answered, `The beautiful young lady at Miss Havisham''s, and she''s more beautiful than anybody ever was, and I admire her dreadfully, and I want to be a gentleman on her account.''',
        ARRAY['english', 'classic'],
        490,
        40,
        2475,
        87
    ),
    (
        'You must thrive in spite of yourself',
        'You must thrive in spite of yourself; and so that you may do it, God takes out your heart of flesh, and gives you a heart of stone.',
        ARRAY[
            'fiction',
            'french',
            'mystery'
        ],
        1424,
        31,
        1511,
        106
    ),
    (
        'He once thought it himself, that he might die',
        'He once thought it himself, that he might die with grief: for his wife, his daughters, his sisters, his father and master the cardinal. But pulse, obdurate, keeps its rhythm. You think you cannot keep breathing, but your ribcage has other ideas, rising and falling, emitting sighs.˝',
        ARRAY['fiction', 'history', 'crime'],
        799,
        4,
        2695,
        59
    ),
    (
        'On foot, from necessity or in deference',
        'On foot, from necessity or in deference to his dismounted commander or associates, his conduct was the same. He would stand like a rock in the open when officers and men alike had taken to cover;',
        ARRAY[
            'fiction',
            'history',
            'classic'
        ],
        1454,
        10,
        4655,
        126
    ),
    (
        'He stood over the body in the fading light',
        'Adjusting the hair and putting the finishing touches to the simple toilet, doing all mechanically, with soulless care. And still through his consciousness ran an undersense of conviction that all was right—that he should have her again as before, and everything explained.',
        ARRAY['classic', 'love', 'american'],
        486,
        31,
        3774,
        125
    ),
    (
        'A secret always has a strengthening effect',
        'A secret always has a strengthening effect upon a newborn friendship, as does the shared impression than an external figure is to blame: the men of the Crown have become united less by their shared beliefs, we observe, than by their shared misgivings.',
        ARRAY[
            'english',
            'magical',
            'french'
        ],
        43,
        32,
        3334,
        150
    ),
    (
        'A judgment that is necessarily hampered',
        'A judgment that is necessarily hampered, bot only by the scope and limits of his imagination, but by the ever-changing measure of his doubt and self-esteem.',
        ARRAY['love', 'english'],
        51,
        39,
        66,
        34
    ),
    (
        'For although a man is judged by his actions',
        'For although a man is judged by his actions, by what he has said and done, a man judges himself by what he is willing to do, by what he might have said, or might have done',
        ARRAY[
            'american',
            'crime',
            'magical'
        ],
        136,
        29,
        240,
        35
    ),
    (
        'Everything failed to subdue me',
        'Everything failed to subdue me. Soon everything seemed dull: another sunrise, the lives of heroes, failing love, war, the discoveries people made about each other.',
        ARRAY[
            'french',
            'american',
            'history'
        ],
        118,
        41,
        269,
        34
    ),
    (
        'In the hospital men''s room',
        'In the hospital men''s room, as I''m washing my hands, I glance in the mirror. The man I see is not so much me as my father. When did he show up? There is no soap; I rub hand sanitizer into my face-it burns. I nearly drown myself in the sink trying to rinse it off.',
        ARRAY[
            'magical',
            'history',
            'fiction'
        ],
        981,
        46,
        1649,
        104
    ),
    (
        'But just as I didn''t want to resent my kids',
        'I also didn''t want to find myself too much in love with them. There are parents who don''t like to hear their little girl crying at night, at the vast approaching dark of sleep, and so in their torment think why not feed her a lollipop, and a few years later that kid''s got seven cavities and a pulled tooth.',
        ARRAY['love', 'magical', 'classic'],
        974,
        36,
        2463,
        126
    ),
    (
        'Christ, he thinks, by my age I ought to know',
        'You don''t get on by being original. You don''t get on by being bright. You don''t get on by being strong. You get on by being a subtle crook;',
        ARRAY[
            'fiction',
            'history',
            'english'
        ],
        143,
        28,
        1460,
        189
    ),
    (
        'They look so fine, and young',
        'They look so fine, and young, and wrapped up in each other. Love is so fresh and clean at that age. Don''t you think?',
        ARRAY[
            'french',
            'classic',
            'history'
        ],
        481,
        21,
        559,
        126
    ),
    (
        'Your only chance of survival',
        'Your only chance of survival, if you are sincerely smitten, lies in hiding this fact from the woman you love, of feigning a casual detachment under all circumstances.',
        ARRAY['crime', 'classic', 'love'],
        1280,
        4,
        3588,
        138
    ),
    (
        'During the first part of your life',
        'During the first part of your life, you only become aware of happiness once you have lost it. Then an age comes, a second one, in which you already know, at the moment when you begin to experience true happiness.',
        ARRAY['love', 'american', 'classic'],
        546,
        42,
        4638,
        56
    ),
    (
        'Were you in love with her?',
        'The question is in a way meaningless, she knows, but one must ask. Love in such situations is rarely real. Sex is the engine, exalting and ruining people, sex and frustration. Love is what people believe is worth the path of devastation.',
        ARRAY[
            'fiction',
            'mystery',
            'english'
        ],
        234,
        10,
        406,
        94
    ),
    (
        'The sun set below the horizon',
        'The sun set below the horizon, and the sky turned a deep shade of purple. The stars began to appear, and the world was bathed in a soft, silvery light. It was a beautiful sight, and for a moment, all was right with the world.',
        ARRAY['magical', 'fiction'],
        243,
        30,
        1157,
        108
    ),
    (
        'He gazed at the old photographs',
        'Memories of a bygone era, each picture telling a story of love, loss, and life. They were his treasures, his link to the past. The sepia tones and worn edges spoke of days long gone, of moments frozen in time that still held so much emotion and significance.',
        ARRAY[
            'memory',
            'history',
            'nostalgia'
        ],
        555,
        16,
        2270,
        57
    ),
    (
        'The forest was alive with the sounds of nature',
        'Birds sang, leaves rustled, and a gentle stream trickled nearby. It was a symphony of life, a reminder of the world''s beauty. The dense canopy overhead filtered the sunlight, creating a mosaic of light and shadow on the forest floor, a tranquil haven far from the chaos of modern life.',
        ARRAY[
            'nature',
            'tranquility',
            'life'
        ],
        366,
        28,
        1868,
        24
    ),
    (
        'She found solace in books',
        'Each page a new adventure, a new world to explore. They were her escape, her refuge from the chaos of reality. Lost in the stories, she traveled to distant lands, met fascinating characters, and experienced the thrill of the unknown, all from the comfort of her favorite chair.',
        ARRAY[
            'books',
            'adventure',
            'escape'
        ],
        297,
        18,
        2529,
        133
    ),
    (
        'The city was a jungle of concrete and steel',
        'But amidst the chaos, there was beauty. The lights, the energy, the endless possibilities. It was alive, vibrant, and ever-changing. Each street had its own rhythm, a pulse that spoke of countless stories unfolding, dreams being chased, and lives intertwining.',
        ARRAY['city', 'urban', 'energy'],
        350,
        42,
        1666,
        37
    ),
    (
        'He marveled at the starry sky',
        'Each star a beacon of light in the vast darkness. It was a reminder of how small we are, and yet how connected. The constellations told ancient stories, while the Milky Way stretched across the sky like a river of light, evoking a sense of wonder and infinite possibilities.',
        ARRAY['stars', 'universe', 'wonder'],
        1053,
        9,
        2898,
        108
    ),
    (
        'The old man told tales of his youth',
        'Stories of adventure, love, and loss. His eyes sparkled with memories, his voice rich with emotion. Each tale was a piece of history, a fragment of a life lived fully, offering lessons learned and wisdom gained through years of experience.',
        ARRAY[
            'stories',
            'memory',
            'emotion'
        ],
        610,
        22,
        3142,
        92
    ),
    (
        'The waves crashed against the rocks',
        'Each surge of water a testament to the ocean''s power and beauty. It was a mesmerizing dance of nature. The relentless rhythm of the waves, their thunderous impact, and the frothy spray all spoke of the untamed force and majesty of the sea.',
        ARRAY['ocean', 'power', 'beauty'],
        1145,
        50,
        3532,
        179
    ),
    (
        'The garden was a riot of color',
        'Flowers of every hue bloomed in a chaotic harmony, a feast for the eyes and a balm for the soul. The air was fragrant with their scent, and the buzzing of bees added a vibrant soundtrack to the dazzling display of natural artistry.',
        ARRAY['garden', 'color', 'harmony'],
        334,
        7,
        4494,
        9
    ),
    (
        'She cherished the quiet moments',
        'Those rare times when the world slowed down, and she could breathe, reflect, and simply be. In the stillness, she found clarity and peace, a chance to reconnect with herself and appreciate the simple joys of life.',
        ARRAY[
            'peace',
            'reflection',
            'serenity'
        ],
        1007,
        12,
        3941,
        108
    ),
    (
        'The music filled the room',
        'Each note a thread in a tapestry of sound, weaving a story of joy, sorrow, and everything in between. The melody carried her away, evoking memories and emotions she thought long forgotten, a powerful reminder of the transformative power of music.',
        ARRAY['music', 'emotion', 'joy'],
        684,
        43,
        4188,
        204
    ),
    (
        'The lighthouse stood tall on the rocky shore',
        'Its beam of light cutting through the thick fog, guiding ships safely to harbor. The lighthouse was a symbol of hope and safety, its solitary presence a beacon for sailors navigating treacherous waters. Its history was etched into the weathered stones, a testament to its enduring purpose.',
        ARRAY[
            'lighthouse',
            'hope',
            'history'
        ],
        223,
        17,
        4608,
        32
    ),
    (
        'She wandered through the ancient ruins',
        'The remnants of a civilization long gone. Each stone, each archway told a story of a bygone era, of people who lived, loved, and dreamed. The air was thick with history, and she felt a deep connection to the past as she explored the crumbling walls.',
        ARRAY['ruins', 'history', 'mystery'],
        183,
        42,
        3098,
        26
    ),
    (
        'The desert stretched out before him',
        'A vast expanse of golden sands under a blazing sun. The silence was profound, broken only by the occasional whisper of the wind. It was a place of stark beauty and harsh reality, where survival required resilience and respect for nature''s power.',
        ARRAY[
            'desert',
            'nature',
            'resilience'
        ],
        400,
        14,
        2002,
        65
    ),
    (
        'The old bridge spanned the wide river',
        'A marvel of engineering from a bygone era. It connected two worlds, carrying the weight of countless journeys and stories. As he walked across, he felt the bridge''s history beneath his feet, a silent witness to the passage of time.',
        ARRAY[
            'bridge',
            'engineering',
            'history'
        ],
        795,
        23,
        1619,
        133
    ),
    (
        'She listened to the rain pattering on the roof',
        'Each drop a gentle reminder of nature''s rhythm. The world outside was a blur of gray, but inside she felt a deep sense of peace. The rain was a lullaby, soothing her mind and soul, a moment of calm in a chaotic world.',
        ARRAY['rain', 'peace', 'nature'],
        105,
        32,
        4351,
        131
    ),
    (
        'The market was a bustling maze of sights and sounds',
        'Stalls filled with colorful produce, the air rich with the scent of spices and fresh flowers. Vendors called out their wares, and the crowd moved in a vibrant dance. It was a place of energy and life, where every visit promised something new.',
        ARRAY['market', 'vibrant', 'life'],
        1165,
        5,
        3654,
        118
    ),
    (
        'The abandoned mansion loomed on the hill',
        'Its once-grand facade now crumbling and overgrown with ivy. It was a place of mystery and melancholy, a relic of a past age. Inside, dust-covered furniture and faded portraits hinted at stories long forgotten, waiting to be discovered.',
        ARRAY[
            'mansion',
            'mystery',
            'history'
        ],
        1119,
        25,
        1652,
        110
    ),
    (
        'The train journey took him through diverse landscapes',
        'From bustling cities to serene countryside. Each view from the window was a snapshot of life, a moving picture of the world outside. The rhythmic clatter of the tracks and the gentle sway of the carriage created a meditative experience.',
        ARRAY[
            'train',
            'journey',
            'landscape'
        ],
        885,
        23,
        1161,
        18
    ),
    (
        'The small village was nestled in a valley',
        'Surrounded by rolling hills and lush greenery. It was a place where time seemed to slow down, where everyone knew each other by name. The village was a tight-knit community, its traditions and stories passed down through generations.',
        ARRAY[
            'village',
            'community',
            'tradition'
        ],
        772,
        17,
        4127,
        208
    ),
    (
        'The library was a haven for book lovers',
        'Rows upon rows of books, each holding a world of knowledge and imagination. The scent of old paper and the quiet rustle of pages created an atmosphere of calm and inspiration. It was a place where stories came to life and minds were set free.',
        ARRAY[
            'library',
            'books',
            'knowledge'
        ],
        67,
        28,
        200,
        46
    ),
    (
        'The meadow was awash with wildflowers',
        'A kaleidoscope of colors under the bright summer sun. Bees buzzed from bloom to bloom, and the air was filled with the sweet scent of flowers. It was a place of pure joy and natural beauty, where one could lose themselves in the simple wonders of the world.',
        ARRAY['meadow', 'flowers', 'nature'],
        1310,
        1,
        4710,
        51
    ),
    (
        'He sat at the piano, fingers poised over the keys',
        'The room was silent, waiting for the first note. As he played, the music flowed, filling the space with melody and emotion. Each piece was a journey, a story told through sound, transporting listeners to places both familiar and unknown.',
        ARRAY['piano', 'music', 'emotion'],
        650,
        44,
        1505,
        175
    ),
    (
        'The farmhouse kitchen was a hub of activity',
        'Pots clanged, and the air was thick with the aroma of home-cooked meals. Family members bustled about, preparing food and sharing stories. It was a place of warmth and togetherness, where traditions were honored and memories were made.',
        ARRAY[
            'kitchen',
            'family',
            'tradition'
        ],
        129,
        18,
        473,
        198
    ),
    (
        'The mountain lake was a mirror',
        'Reflecting the snow-capped peaks and clear blue sky. The water was crystal clear, inviting and serene. It was a place of tranquility, where one could sit on the shore and lose themselves in the beauty of nature''s perfection.',
        ARRAY[
            'lake',
            'mountain',
            'serenity'
        ],
        1489,
        48,
        4102,
        128
    ),
    (
        'The city''s historic district was a blend of old and new',
        'Cobblestone streets and centuries-old buildings stood alongside modern cafes and boutiques. It was a place where history met contemporary life, creating a unique and vibrant atmosphere. Every corner had a story to tell, a piece of the past woven into the present.',
        ARRAY['city', 'history', 'vibrant'],
        1015,
        35,
        1206,
        80
    ),
    (
        'The library''s grand reading room',
        'Was a sanctuary of knowledge and quiet. High ceilings and towering shelves filled with books created a majestic space. Here, scholars and dreamers alike could find inspiration, lose themselves in stories, and explore the vast realms of human thought.',
        ARRAY[
            'library',
            'knowledge',
            'inspiration'
        ],
        793,
        24,
        966,
        112
    ),
    (
        'The beach at sunset was a sight to behold',
        'The sky transformed into a canvas of vibrant colors, each wave catching the light in a dance of gold and crimson. Couples strolled along the shore, hand in hand, as children played at the water''s edge. It was a moment of pure beauty, a day ending with a promise of tomorrow.',
        ARRAY['beach', 'sunset', 'beauty'],
        174,
        30,
        197,
        201
    ),
    (
        'The old theater was a relic of a glamorous past',
        'Its ornate decorations and grand stage spoke of a time when it was the heart of the city''s cultural scene. Even in its faded glory, it retained a charm that drew visitors in, sparking imaginations and whispering tales of performances long past.',
        ARRAY[
            'theater',
            'history',
            'culture'
        ],
        1128,
        1,
        4973,
        83
    ),
    (
        'The garden was a sanctuary',
        'A place where she could escape the stresses of daily life. Surrounded by greenery and the sound of birdsong, she found peace and solace. Each plant, each bloom was a testament to her care and love, a living tapestry of colors and scents.',
        ARRAY[
            'garden',
            'peace',
            'sanctuary'
        ],
        282,
        42,
        704,
        195
    ),
    (
        'The bookshop was a treasure trove',
        'Every shelf crammed with stories waiting to be discovered. The scent of old books and the quiet atmosphere created a haven for book lovers. It was a place where time stood still, and one could lose themselves in the magic of reading.',
        ARRAY['bookshop', 'books', 'magic'],
        1231,
        10,
        2367,
        36
    ),
    (
        'The orchard was in full bloom',
        'Rows of fruit trees stood adorned with delicate blossoms, their petals falling like confetti in the gentle breeze. The air was fragrant with the promise of a bountiful harvest, and the soft hum of bees added a musical backdrop to the idyllic scene.',
        ARRAY[
            'orchard',
            'blossom',
            'nature'
        ],
        565,
        16,
        2732,
        44
    ),
    (
        'The bustling marketplace',
        'Was a sensory overload of colors, sounds, and scents. Vendors called out their wares, and the crowd moved in a vibrant dance. Fresh produce, handmade crafts, and exotic spices filled the stalls, each one a testament to the rich culture and community spirit.',
        ARRAY[
            'marketplace',
            'vibrant',
            'community'
        ],
        795,
        11,
        798,
        2
    ),
    (
        'The old journal revealed secrets of the past',
        'Its pages filled with handwritten notes, sketches, and pressed flowers. It was a treasure trove of memories, a window into the life of its owner. Each entry told a story, offering glimpses into moments of joy, sorrow, and everyday life from a different time.',
        ARRAY[
            'journal',
            'memories',
            'secrets'
        ],
        589,
        16,
        670,
        185
    ),
    (
        'The beach was a paradise',
        'Golden sands stretched as far as the eye could see, meeting the turquoise waters of the ocean. Palm trees swayed gently in the breeze, and the sound of waves crashing against the shore was a soothing symphony. It was a place where worries melted away under the sun.',
        ARRAY['beach', 'paradise', 'ocean'],
        783,
        42,
        1303,
        201
    ),
    (
        'The village fair was in full swing',
        'Brightly colored tents and booths lined the streets, offering games, food, and crafts. Children ran about with cotton candy, while adults enjoyed the live music and entertainment. It was a day of fun and community spirit, where everyone came together to celebrate.',
        ARRAY[
            'fair',
            'celebration',
            'community'
        ],
        727,
        47,
        2308,
        26
    ),
    (
        'The cabin in the woods',
        'Was a perfect retreat from the hustle and bustle of everyday life. Surrounded by towering trees and the sounds of nature, it offered peace and solitude. Inside, the rustic charm of wooden beams and a roaring fireplace made it a cozy haven.',
        ARRAY['cabin', 'woods', 'retreat'],
        718,
        4,
        1279,
        11
    ),
    (
        'The art gallery was a feast for the eyes',
        'Each piece a masterpiece, telling a story through color, form, and texture. Visitors moved from one exhibit to another, lost in contemplation. The gallery was a place of inspiration and creativity, where art came alive and sparked the imagination.',
        ARRAY[
            'art',
            'gallery',
            'inspiration'
        ],
        362,
        44,
        4619,
        162
    ),
    (
        'The evening concert was magical',
        'Under the starry sky, the music flowed, captivating the audience. The performers poured their hearts into each note, creating a tapestry of sound that resonated with everyone present. It was a night of shared joy and musical wonder.',
        ARRAY['concert', 'music', 'magic'],
        890,
        29,
        1606,
        167
    ),
    (
        'The farm was a bustling hub of activity',
        'Animals roamed the fields, and the air was filled with the sounds of clucking chickens and lowing cows. Crops swayed in the breeze, promising a good harvest. It was a place of hard work and satisfaction, where the rhythm of life followed the seasons.',
        ARRAY['farm', 'activity', 'life'],
        463,
        13,
        488,
        188
    ),
    (
        'The mountain trail was challenging',
        'But the views from the top were breathtaking. Each step was a test of endurance, but reaching the summit made it all worthwhile. The panorama of rugged peaks and valleys stretched out before him, a reward for his determination and effort.',
        ARRAY[
            'mountain',
            'trail',
            'challenge'
        ],
        62,
        15,
        4432,
        134
    ),
    (
        'The hidden cove was a secret paradise',
        'Tucked away from the main beach, it was a tranquil spot where crystal-clear waters met soft, white sands. The sound of gentle waves and the occasional call of a seagull created a serene atmosphere. It was a perfect place to escape and soak in the natural beauty.',
        ARRAY[
            'cove',
            'paradise',
            'serenity'
        ],
        1050,
        17,
        1535,
        183
    ),
    (
        'The grand ballroom',
        'Was a scene of elegance and grandeur. Crystal chandeliers hung from the ceiling, casting a warm glow over the polished marble floor. Guests in their finest attire danced to the orchestra, their movements a graceful dance of opulence and refinement.',
        ARRAY[
            'ballroom',
            'elegance',
            'grandeur'
        ],
        977,
        7,
        1648,
        82
    ),
    (
        'The ancient castle stood atop the hill',
        'Its towering walls and turrets a testament to a rich history. Ivy crept up its stone facade, and inside, the halls echoed with the footsteps of those long gone. Each room held artifacts and stories, making it a living museum of the past.',
        ARRAY[
            'castle',
            'history',
            'ancient'
        ],
        978,
        4,
        1103,
        145
    ),
    (
        'The river flowed gently through the valley',
        'Its waters sparkling in the sunlight. Along its banks, wildflowers bloomed, and birds sang from the trees. It was a place of natural beauty and calm, where one could sit by the water''s edge and feel at peace with the world.',
        ARRAY['river', 'valley', 'calm'],
        152,
        40,
        4379,
        181
    ),
    (
        'The bustling harbor was alive with activity',
        'Ships of all sizes came and went, and the air was filled with the sound of seagulls and the smell of the sea. Fishermen unloaded their catch, while tourists explored the nearby shops and cafes. It was a vibrant, dynamic place where life never stood still.',
        ARRAY[
            'harbor',
            'activity',
            'vibrant'
        ],
        509,
        48,
        1715,
        164
    ),
    (
        'The summer festival',
        'Transformed the town into a carnival of lights and sounds. Streets were lined with stalls offering food, games, and crafts. Music filled the air, and the laughter of children added to the festive atmosphere. It was a time of joy and community spirit, where everyone came together to celebrate.',
        ARRAY[
            'festival',
            'summer',
            'celebration'
        ],
        167,
        20,
        187,
        150
    ),
    (
        'The autumn forest',
        'Was a riot of color, with leaves in shades of red, orange, and yellow. The crisp air and the crunch of leaves underfoot made it a perfect place for a walk. Each tree seemed to tell its own story, and the changing seasons added a sense of wonder to the landscape.',
        ARRAY['forest', 'autumn', 'color'],
        67,
        4,
        908,
        156
    ),
    (
        'The old lighthouse',
        'Had stood for centuries, its light guiding sailors safely to shore. Perched on a rocky cliff, it faced the relentless waves with steadfast determination. Inside, the spiral staircase led to a room with a panoramic view of the ocean, a reminder of the lighthouse''s vital role.',
        ARRAY[
            'lighthouse',
            'ocean',
            'history'
        ],
        430,
        41,
        3875,
        83
    ),
    (
        'The snow-covered village',
        'Looked like a scene from a postcard. Smoke curled from chimneys, and the streets were lined with festive decorations. Children played in the snow, building snowmen and sledding down hills. It was a picture of warmth and community spirit, despite the cold weather.',
        ARRAY[
            'village',
            'snow',
            'community'
        ],
        967,
        3,
        3600,
        127
    ),
    (
        'The botanical garden',
        'Was a haven of peace and beauty. Pathways meandered through beds of exotic plants and flowers, each one labeled with its name and origin. The scent of blooms filled the air, and the gentle sound of water features added to the tranquil atmosphere.',
        ARRAY[
            'garden',
            'botanical',
            'beauty'
        ],
        651,
        29,
        2249,
        93
    ),
    (
        'The starlit sky dazzled above the desert',
        'Each twinkling light seemed to tell a story of its own, as if the universe itself was whispering secrets. The vastness of space stretched out endlessly, filling the night with a sense of wonder and possibility. It was a sight that made one feel both small and connected to something greater.',
        ARRAY['desert', 'stars', 'wonder'],
        1275,
        7,
        3157,
        159
    ),
    (
        'The coastal cliffs towered over the crashing waves',
        'Their rugged beauty a testament to the power of the ocean. Seabirds circled overhead, their cries carried by the salty breeze. Down below, tide pools teemed with life, hidden gems waiting to be discovered by adventurous explorers. It was a place where land and sea converged in a breathtaking display of nature''s grandeur.',
        ARRAY['coast', 'cliffs', 'nature'],
        230,
        32,
        3011,
        102
    ),
    (
        'The quaint village square bustled with activity',
        'Market stalls lined the cobblestone streets, offering fresh produce, handmade crafts, and local delicacies. Children played games in the fountain, their laughter mingling with the chatter of residents and tourists alike. It was a scene straight out of a storybook, where time seemed to stand still amidst the timeless charm of the village.',
        ARRAY['village', 'square', 'charm'],
        20,
        14,
        532,
        19
    ),
    (
        'The ancient temple stood silent in the jungle',
        'Its weathered stone walls adorned with intricate carvings and statues of forgotten gods. Vines and moss clung to the ruins, reclaiming them for nature. Despite the passage of centuries, there was a palpable sense of reverence and spirituality in the air, as if the spirits of the past still lingered among the crumbling stones.',
        ARRAY[
            'temple',
            'jungle',
            'spirituality'
        ],
        248,
        44,
        2561,
        174
    ),
    (
        'The mountain stream sparkled in the sunlight',
        'Its crystal-clear waters bubbling over smooth rocks and pebbles. Tall trees shaded the banks, their leaves rustling in the gentle breeze. The air was fresh and invigorating, carrying with it the scent of pine and wildflowers. It was a scene of pure serenity, a hidden gem tucked away in the heart of the wilderness.',
        ARRAY[
            'mountain',
            'stream',
            'serenity'
        ],
        1193,
        13,
        4993,
        142
    ),
    (
        'The bustling city market was a melting pot of cultures',
        'Vendors from around the world offered spices, textiles, and handicrafts, each item a reflection of its creator''s heritage. The air was filled with the aroma of exotic foods cooking on open grills, tempting passersby with their savory scents. It was a vibrant celebration of diversity, where strangers became friends over shared experiences and shared meals.',
        ARRAY['city', 'market', 'diversity'],
        1391,
        29,
        2741,
        157
    ),
    (
        'The old oak tree stood sentinel in the meadow',
        'Its gnarled branches reaching towards the sky, a silent witness to the passage of time. Birds nested in its leafy canopy, and squirrels darted among its roots. Beneath its shade, wildflowers bloomed, adding splashes of color to the verdant landscape. It was a symbol of strength and resilience, a constant presence in an ever-changing world.',
        ARRAY['oak', 'meadow', 'strength'],
        438,
        10,
        3943,
        61
    ),
    (
        'The traditional tea house exuded warmth and hospitality',
        'Guests were greeted with a smile and offered a seat at low wooden tables. The aroma of freshly brewed tea filled the air, mingling with the soft sounds of conversation and laughter. It was a place of relaxation and connection, where strangers became friends over shared cups of tea and shared stories.',
        ARRAY[
            'tea',
            'hospitality',
            'connection'
        ],
        369,
        50,
        2207,
        155
    ),
    (
        'The rural farmstead was a picture of idyllic simplicity',
        'Fields of golden wheat stretched to the horizon, swaying in the gentle breeze. Cows grazed lazily in lush green pastures, and chickens pecked at the ground in search of insects. The air was filled with the scent of fresh hay and the distant sound of a tractor chugging along. It was a scene straight out of a pastoral painting, a timeless reminder of the beauty of rural life.',
        ARRAY[
            'farmstead',
            'rural',
            'simplicity'
        ],
        939,
        32,
        4455,
        180
    ),
    (
        'The majestic waterfall cascaded down the cliffside',
        'Its thunderous roar drowned out all other sounds, filling the air with energy and vitality. Mist rose from the churning pool below, creating rainbows in the sunlight. Around the waterfall, lush vegetation thrived, nourished by the constant spray. It was a spectacle of nature''s power and beauty, a sight that left all who beheld it in awe.',
        ARRAY[
            'waterfall',
            'majestic',
            'nature'
        ],
        412,
        21,
        2924,
        12
    ),
    (
        'The ancient ruins whispered tales of the past',
        'Stone columns stood like silent sentinels, their weathered surfaces bearing the scars of time. In the midst of the ruins, a lone statue remained, its features worn but still dignified. It was a place where history came alive, where the echoes of ancient civilizations lingered in the air.',
        ARRAY['ruins', 'history', 'ancient'],
        603,
        38,
        3061,
        130
    ),
    (
        'The sun-dappled forest was alive with the chatter of birds',
        'Tall trees stretched towards the sky, their branches forming a canopy overhead. Shafts of golden light filtered through the leaves, illuminating the forest floor in a patchwork of light and shadow. It was a place of tranquility and beauty, where one could lose themselves in the wonders of nature.',
        ARRAY[
            'forest',
            'nature',
            'tranquility'
        ],
        593,
        6,
        3935,
        122
    ),
    (
        'The coastal town was a haven for seafood lovers',
        'Fishermen returned from sea with their catch of the day, which was then sold at the bustling fish market. Restaurants lined the waterfront, offering freshly prepared dishes that showcased the bounty of the ocean. It was a place where the salty tang of the sea lingered in the air and every meal was a celebration of coastal living.',
        ARRAY['coastal', 'seafood', 'food'],
        7,
        5,
        12,
        48
    ),
    (
        'The ancient cathedral soared towards the heavens',
        'Its towering spires reached for the sky, a testament to the faith and craftsmanship of generations past. Stained glass windows bathed the interior in a kaleidoscope of colors, and the air was filled with the soft murmur of prayers. It was a place of awe and reverence, where the divine felt close at hand.',
        ARRAY[
            'cathedral',
            'faith',
            'architecture'
        ],
        470,
        41,
        2301,
        47
    ),
    (
        'The vibrant street market buzzed with energy',
        'Stalls overflowed with fruits, vegetables, and spices from around the world, creating a riot of color and scent. Street performers entertained the crowds with music and dance, while vendors called out their wares in a cacophony of voices. It was a place where the pulse of the city could be felt most keenly, a melting pot of cultures and commerce.',
        ARRAY['market', 'street', 'vibrant'],
        1114,
        17,
        3147,
        6
    ),
    (
        'The old windmill stood as a symbol of bygone days',
        'Its weathered sails creaked and groaned in the wind, a reminder of a time when such structures dotted the countryside. Inside, the machinery lay dormant, its purpose long forgotten. Yet the windmill remained a beloved landmark, a link to the past in an ever-changing world.',
        ARRAY[
            'windmill',
            'history',
            'countryside'
        ],
        2,
        0,
        36,
        120
    ),
    (
        'The bustling city street was a feast for the senses',
        'Neon lights flashed, and the air was thick with the scent of street food cooking on open grills. Pedestrians jostled for space on crowded sidewalks, their voices blending into a vibrant symphony of urban life. It was a place where every corner held a new adventure, where the pulse of the city could be felt in every heartbeat.',
        ARRAY['city', 'street', 'urban'],
        212,
        15,
        1903,
        2
    ),
    (
        'The rolling hills stretched to the horizon',
        'Dotted with sheep grazing peacefully in the meadows. Fences crisscrossed the landscape, dividing fields of green and gold. In the distance, a lone farmhouse stood silhouetted against the sky. It was a scene of pastoral tranquility, where time seemed to slow and the worries of the world faded away.',
        ARRAY[
            'hills',
            'pastoral',
            'tranquility'
        ],
        1333,
        0,
        2460,
        184
    ),
    (
        'The quaint village inn welcomed weary travelers',
        'Its cozy rooms and warm hearth offered respite from the road. In the tavern below, locals gathered to share stories over pints of ale, their laughter echoing through the rafters. It was a place where strangers became friends and memories were made, a home away from home for those in need of rest.',
        ARRAY[
            'village',
            'inn',
            'hospitality'
        ],
        918,
        50,
        1939,
        174
    ),
    (
        'The ancient forest was shrouded in mystery',
        'Moss-covered trees loomed overhead, their twisted branches reaching towards the sky like skeletal fingers. The forest floor was carpeted in ferns and fallen leaves, and shafts of sunlight pierced the canopy in ethereal beams. It was a place where legends were born and adventurers dared to tread, drawn by the promise of untold treasures hidden in its depths.',
        ARRAY[
            'forest',
            'mystery',
            'adventure'
        ],
        159,
        7,
        865,
        61
    ),
    (
        'The coastal cliffs provided a breathtaking view',
        'Stretching for miles along the rugged coastline, the cliffs rose majestically from the crashing waves below. Seabirds circled overhead, riding the ocean breezes, while below, hidden coves and sea caves beckoned adventurous explorers. It was a place where the raw power of nature met the beauty of the sea.',
        ARRAY['coastal', 'cliffs', 'view'],
        532,
        50,
        4134,
        163
    ),
    (
        'The bustling city square was the heart of urban life',
        'Surrounded by historic buildings and modern skyscrapers, the square buzzed with activity day and night. Street performers entertained crowds, while vendors sold their wares from colorful stalls. It was a place where cultures collided and history was written with every step.',
        ARRAY['city', 'square', 'urban'],
        60,
        12,
        127,
        199
    ),
    (
        'The ancient aqueducts stood as a testament to engineering prowess',
        'Stretching across the landscape, the aqueducts carried water from distant sources to the bustling cities of old. Their arches soared high above the ground, a marvel of Roman ingenuity and craftsmanship. Even in ruins, they remained a symbol of civilization''s triumph over nature.',
        ARRAY[
            'aqueducts',
            'engineering',
            'history'
        ],
        468,
        9,
        4614,
        170
    ),
    (
        'The remote mountain village was a hidden gem',
        'Nestled among towering peaks and lush forests, the village seemed untouched by time. Its cobblestone streets and ancient buildings whispered tales of a simpler era, while the surrounding landscape offered endless opportunities for adventure. It was a place where modern life felt a world away.',
        ARRAY[
            'mountain',
            'village',
            'remote'
        ],
        1035,
        45,
        3022,
        134
    ),
    (
        'The tranquil lake mirrored the surrounding mountains',
        'Its glassy surface reflecting the blue sky above and the green trees along its shores. Ducks glided across the water, leaving ripples in their wake, while fishermen cast their lines from the wooden pier. It was a scene of timeless beauty, where nature''s harmony was on full display.',
        ARRAY['lake', 'tranquil', 'nature'],
        22,
        0,
        1913,
        182
    ),
    (
        'The historic castle loomed over the town below',
        'Its stone walls and imposing towers a reminder of a bygone era. Inside, tapestries lined the walls, and suits of armor stood sentinel in the grand hall. From the ramparts, one could see for miles in every direction, a view that spoke of centuries of power and prestige.',
        ARRAY[
            'castle',
            'history',
            'fortress'
        ],
        226,
        7,
        423,
        7
    ),
    (
        'The bustling harbor was a hive of activity',
        'Ships of all sizes came and went, their colorful sails billowing in the wind. Fishermen unloaded their catch, while dockworkers bustled about, loading and unloading cargo. It was a place where the rhythm of life was dictated by the tides, and every day brought new adventures on the high seas.',
        ARRAY['harbor', 'activity', 'sea'],
        158,
        46,
        4339,
        81
    ),
    (
        'The charming cottage nestled in a sun-dappled clearing',
        'Surrounded by a riot of flowers and greenery, the cottage seemed straight out of a fairy tale. Smoke curled from the chimney, and the sound of birdsong filled the air. It was a place of peace and tranquility, where time moved at its own pace.',
        ARRAY[
            'cottage',
            'charming',
            'peace'
        ],
        352,
        30,
        3264,
        138
    ),
    (
        'The bustling market square was a melting pot of cultures',
        'Vendors from far and wide gathered to sell their goods, their voices blending into a cacophony of languages and dialects. Spices, textiles, and handicrafts filled the stalls, creating a riot of color and scent. It was a place where the world came together, united by the shared experience of trade and commerce.',
        ARRAY['market', 'culture', 'trade'],
        1234,
        9,
        3774,
        29
    ),
    (
        'The ancient forest whispered secrets to those who dared to listen',
        'Tall trees swayed in the breeze, their branches reaching towards the sky like fingers seeking the heavens. Moss-covered stones dotted the forest floor, marking the passage of time. It was a place of magic and mystery, where legends walked among the shadows.',
        ARRAY[
            'forest',
            'ancient',
            'mystery'
        ],
        1315,
        4,
        3719,
        175
    ),
    (
        'The sun-kissed vineyard stretched across the hills',
        'Rows of grapevines marched in perfect formation, their leaves rustling in the gentle breeze. The air was heavy with the scent of ripe fruit, and the distant sound of laughter echoed through the valley. It was a place where time seemed to stand still, and every sip of wine was a toast to the beauty of life.',
        ARRAY['vineyard', 'hills', 'wine'],
        1104,
        35,
        2884,
        89
    ),
    (
        'The historic town square was a living museum',
        'Surrounded by centuries-old buildings, the square bore witness to generations of history. A statue of a local hero stood at its center, immortalized in bronze. Cafes and shops lined the cobblestone streets, each one a testament to the enduring spirit of the town.',
        ARRAY['town', 'square', 'history'],
        1046,
        12,
        1576,
        172
    ),
    (
        'The tranquil pond was a haven for wildlife',
        'Water lilies floated serenely on the surface, their delicate blooms a splash of color against the greenery. Ducks paddled lazily in the shallows, while fish darted beneath the surface. It was a place where nature thrived, untouched by the chaos of the world beyond.',
        ARRAY[
            'pond',
            'wildlife',
            'tranquil'
        ],
        261,
        37,
        4219,
        189
    ),
    (
        'The bustling city market was a feast for the senses',
        'Stalls overflowed with exotic spices, fresh produce, and handmade crafts from around the world. The air was thick with the scent of roasting coffee and sizzling street food. It was a place where the pulse of the city could be felt most keenly, where every corner held a new adventure.',
        ARRAY['city', 'market', 'senses'],
        1457,
        15,
        3074,
        88
    ),
    (
        'The picturesque waterfall cascaded down the rocky cliffs',
        'Its pristine waters shimmered in the sunlight, creating rainbows in the mist. Ferns and moss clung to the rocks, adding to the sense of natural beauty. It was a place where the power of nature was on full display, where every drop of water was a testament to the earth''s resilience.',
        ARRAY[
            'waterfall',
            'picturesque',
            'nature'
        ],
        666,
        33,
        2960,
        35
    ),
    (
        'The cozy cabin nestled in the snowy mountains',
        'Smoke curled from the chimney, and the sound of crackling logs filled the air. Outside, snowflakes drifted lazily to the ground, transforming the landscape into a winter wonderland. It was a place where warmth and comfort awaited, a sanctuary from the cold embrace of winter.',
        ARRAY['cabin', 'mountains', 'snowy'],
        1303,
        27,
        4934,
        178
    ),
    (
        'The ancient library was a treasure trove of knowledge',
        'Books lined the shelves from floor to ceiling, their leather-bound covers worn with age. Dust motes danced in the sunlight that streamed through stained glass windows. It was a place where scholars and seekers alike came to lose themselves in the wisdom of the ages.',
        ARRAY[
            'library',
            'ancient',
            'knowledge'
        ],
        491,
        24,
        2605,
        192
    ),
    (
        'The vibrant city park was a green oasis',
        'Towering trees provided shade from the sun, while colorful flowers bloomed in every corner. Children laughed and played on the playground, while couples strolled hand in hand along winding paths. It was a place where the beauty of nature and the energy of the city came together in perfect harmony.',
        ARRAY['park', 'city', 'oasis'],
        623,
        8,
        1466,
        83
    ),
    (
        'The rustic farmhouse stood amidst fields of golden wheat',
        'Its weathered facade bore the marks of countless seasons, yet it stood strong and proud against the elements. Chickens scratched in the dirt, and cows grazed lazily in the pasture. It was a place where the simple pleasures of life were cherished, and hard work was its own reward.',
        ARRAY[
            'farmhouse',
            'rustic',
            'fields'
        ],
        117,
        42,
        2041,
        205
    ),
    (
        'The sunlit garden was a riot of color',
        'Flowers of every hue bloomed in profusion, their petals kissed by the golden rays of the sun. Bees buzzed from blossom to blossom, gathering nectar for their hive. It was a place where beauty and life abounded, where the passage of time was marked by the changing seasons.',
        ARRAY[
            'garden',
            'sunlit',
            'colorful'
        ],
        96,
        28,
        276,
        90
    ),
    (
        'The serene meadow stretched to the horizon',
        'Dotted with wildflowers and bathed in golden sunlight, the meadow was a picture of tranquility. Butterflies flitted from bloom to bloom, while bees hummed lazily in the warm air. It was a place where time seemed to stand still, a sanctuary from the chaos of the world.',
        ARRAY[
            'meadow',
            'serene',
            'tranquility'
        ],
        142,
        34,
        1197,
        60
    ),
    (
        'The quaint village church stood as a beacon of faith',
        'Its steeple reached towards the heavens, a symbol of hope and salvation. Inside, shafts of sunlight streamed through stained glass windows, casting colorful patterns on the worn wooden pews. It was a place where the weary found solace and the lost found direction.',
        ARRAY['church', 'village', 'faith'],
        1040,
        46,
        3368,
        150
    ),
    (
        'The ancient ruins were a window into the past',
        'Stone pillars stood in silent testimony to the glory of civilizations long gone. Crumbling walls bore the scars of battles fought and empires risen and fallen. It was a place where history whispered its secrets to those who dared to listen.',
        ARRAY['ruins', 'history', 'ancient'],
        846,
        36,
        4333,
        58
    ),
    (
        'The tranquil river wound its way through the countryside',
        'Its waters flowed lazily, reflecting the blue sky above and the green trees along its banks. Fish darted beneath the surface, and dragonflies flitted above the reeds. It was a place where one could escape the noise of the world and find peace in the gentle rhythm of nature.',
        ARRAY[
            'river',
            'tranquil',
            'countryside'
        ],
        891,
        25,
        1526,
        196
    ),
    (
        'The bustling city streets were alive with energy',
        'People hurried along the sidewalks, their footsteps echoing off the towering buildings. Cars honked and sirens wailed, creating a symphony of urban sounds. It was a place where dreams were chased and fortunes made, where every corner held the promise of adventure.',
        ARRAY['city', 'streets', 'energy'],
        254,
        37,
        1906,
        88
    ),
    (
        'The cozy bookstore was a haven for book lovers',
        'Shelves lined with books stretched from floor to ceiling, each one a portal to another world. The scent of paper and ink filled the air, and the sound of pages turning was like music to the ears. It was a place where imaginations soared and hearts found refuge.',
        ARRAY['bookstore', 'books', 'cozy'],
        1139,
        11,
        2054,
        132
    ),
    (
        'The vibrant city skyline glittered in the night',
        'Skyscrapers towered overhead, their lights twinkling like stars against the darkness. Below, the streets pulsed with life, as cars and pedestrians moved in a constant dance. It was a place where dreams took flight and the possibilities seemed endless.',
        ARRAY['city', 'skyline', 'night'],
        849,
        38,
        2109,
        93
    ),
    (
        'The ancient temple stood as a testament to human ingenuity',
        'Its intricate carvings and towering columns spoke of a time when gods walked among mortals. Inside, priests tended to sacred fires, their chants filling the air with reverence. It was a place where the divine felt close at hand, where prayers were offered and answered.',
        ARRAY['temple', 'ancient', 'faith'],
        714,
        6,
        3308,
        149
    ),
    (
        'The peaceful village square was a gathering place for locals',
        'Beneath the shade of a towering oak tree, villagers gathered to chat and share stories. Children played games on the cobblestone streets, while elders watched with fond smiles. It was a place where time seemed to move more slowly, and the simple joys of life were savored.',
        ARRAY[
            'village',
            'square',
            'peaceful'
        ],
        67,
        34,
        1836,
        52
    ),
    (
        'The majestic waterfall thundered down the mountainside',
        'Its waters cascading in a frothy torrent, sending mist into the air. Rainbows danced in the spray, adding to the spectacle of nature''s power. It was a place where one could feel the heartbeat of the earth, a reminder of the awe-inspiring beauty of the natural world.',
        ARRAY[
            'waterfall',
            'majestic',
            'nature'
        ],
        703,
        33,
        867,
        187
    );