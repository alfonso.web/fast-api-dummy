from datetime import datetime, timezone
from enum import Enum
from typing import ClassVar, List, Optional, Set, Union
from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, Json, field_validator
from sqlalchemy import ARRAY, String
from sqlmodel import Field, Relationship, SQLModel, Column, text, TIMESTAMP


class GenderEnum(str, Enum):
    female = "female"
    male = "male"


def datetime_now() -> datetime:
    return datetime.now(timezone.utc)


# JSON payload containing access token
class Token(SQLModel):
    access_token: str
    token_type: str = "bearer"


# Contents of JWT token
class TokenPayload(SQLModel):
    sub: int | None = None


class TokenData(BaseModel):
    username: Union[str, None] = None


# Generic message
class Message(SQLModel):
    message: str


class NewPassword(SQLModel):
    token: str
    new_password: str


class UserBase(SQLModel):
    first_name: str
    last_name: str
    username: str = Field(unique=True, index=True)
    email: EmailStr = Field(unique=True, index=True)
    gender: GenderEnum = GenderEnum.male


class User(UserBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    created_at: datetime = Field(
        default_factory=datetime_now,
        sa_column=Column(
            TIMESTAMP(timezone=True),
            nullable=False,
            server_default=text("CURRENT_TIMESTAMP"),
            server_onupdate=text("CURRENT_TIMESTAMP"),
        ),
    )
    password: str
    posts: List["Post"] = Relationship(back_populates="user")
    comments: List["Comment"] = Relationship(back_populates="user")


class UserRegister(UserBase):
    email: EmailStr = Field(unique=True, index=True)
    password: str

    @field_validator("password")
    @classmethod
    def validate_password(cls, value):
        if len(value) < 8:
            raise HTTPException(
                status_code=422, detail="Password must be at least 8 characters long"
            )
        return value


# TODO replace email str with EmailStr when sqlmodel supports it
class UserUpdateEmail(SQLModel):
    email: EmailStr = Field(unique=True, index=True)


class UserUpdatePassword(SQLModel):
    current_password: str
    new_password: str


class UserPublic(UserBase):
    id: int


class PostBase(SQLModel):
    title: str
    body: str
    tags: list[str] = Field(sa_column=Column(ARRAY(String), nullable=True))
    likes: int | None = Field(default=0)
    dislikes: int | None = Field(default=0)
    views: int | None = Field(default=0)
    created_at: datetime = Field(
        default_factory=datetime_now,
        sa_column=Column(
            TIMESTAMP(timezone=True),
            nullable=False,
            server_default=text("CURRENT_TIMESTAMP"),
            server_onupdate=text("CURRENT_TIMESTAMP"),
        ),
    )


class Post(PostBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    created_at: datetime = Field(
        default_factory=datetime_now,
        sa_column=Column(
            TIMESTAMP(timezone=True),
            nullable=False,
            server_default=text("CURRENT_TIMESTAMP"),
            server_onupdate=text("CURRENT_TIMESTAMP"),
        ),
    )

    user_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    user: User | None = Relationship(back_populates="posts")
    comments: List["Comment"] = Relationship(back_populates="post")


class PostPublic(PostBase):
    id: int
    user: UserPublic | None = None


class PostsPublic(SQLModel):
    data: list[PostPublic]
    count: int


class PostCreate(PostBase):
    title: str
    body: str
    tags: list[str] = Field(sa_column=Column(ARRAY(String), nullable=True))


class PostUpdate(SQLModel):
    title: str
    body: str
    tags: list[str] = Field(sa_column=Column(ARRAY(String), nullable=True))


class PostUpdateLike(SQLModel):
    likes: int


class CommentBase(SQLModel):
    body: str
    likes: int | None = Field(default=0)


class Comment(CommentBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    post_id: int | None = Field(default=None, foreign_key="post.id", nullable=False)
    post: Post | None = Relationship(back_populates="comments")
    created_at: datetime = Field(
        default_factory=datetime_now,
        sa_column=Column(
            TIMESTAMP(timezone=True),
            nullable=False,
            server_default=text("CURRENT_TIMESTAMP"),
            server_onupdate=text("CURRENT_TIMESTAMP"),
        ),
    )

    user_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    user: User | None = Relationship(back_populates="comments")


class CommentCreate(CommentBase):
    post_id: int


class CommentPublic(CommentBase):
    id: int
    user: UserPublic | None = None


class CommentUpdate(SQLModel):
    body: str
