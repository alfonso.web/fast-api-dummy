FROM python:latest

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY ./app ./app

# CMD ["gunicorn", "app.main:app", "--bind", "0.0.0.0:5000", "-k", "uvicorn.workers.UvicornWorker"]
# gunicorn app.main:app --bind 0.0.0.0:5000 -k uvicorn.workers.UvicornWorker
# CMD ["fastapi", "run", "app/main.py", "--port", "8000","--reload"]